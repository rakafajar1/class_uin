import 'dart:convert';

ClassroomDetail classromDetailFromJson(String str) =>
    ClassroomDetail.fromJson(json.decode(str));

String classromDetailToJson(ClassroomDetail data) => json.encode(data.toJson());

class ClassroomDetail {
  ClassroomDetail({
    this.status,
    this.message,
    this.content,
    this.meta,
  });

  int status;
  String message;
  List<ContentClassDetail> content;
  MetaClassPage meta;

  factory ClassroomDetail.fromJson(Map<String, dynamic> json) =>
      ClassroomDetail(
        status: json["status"],
        message: json["message"],
        content: List<ContentClassDetail>.from(
            json["content"].map((x) => ContentClassDetail.fromJson(x))),
        meta: MetaClassPage.fromJson(json["meta"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "content": List<dynamic>.from(content.map((x) => x.toJson())),
        "meta": meta.toJson(),
      };
}

class ContentClassDetail {
  ContentClassDetail({
    this.id,
    this.studentId,
    this.studentName,
    this.indicators,
    this.totalIndicatorScore,
  });

  int id;
  int studentId;
  String studentName;
  int indicators;
  double totalIndicatorScore;

  factory ContentClassDetail.fromJson(Map<String, dynamic> json) =>
      ContentClassDetail(
        id: json["id"],
        studentId: json["student_id"],
        studentName: json["student_name"],
        indicators: json["indicators"],
        totalIndicatorScore: json["total_indicator_score"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "student_id": studentId,
        "student_name": studentName,
        "indicators": indicators,
        "total_indicator_score": totalIndicatorScore,
      };
}

class MetaClassPage {
  MetaClassPage({
    this.total,
    this.currentPage,
    this.lastPage,
    this.perPage,
  });

  int total;
  int currentPage;
  int lastPage;
  int perPage;

  factory MetaClassPage.fromJson(Map<String, dynamic> json) => MetaClassPage(
        total: json["total"],
        currentPage: json["current_page"],
        lastPage: json["last_page"],
        perPage: json["per_page"],
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "current_page": currentPage,
        "last_page": lastPage,
        "per_page": perPage,
      };
}
