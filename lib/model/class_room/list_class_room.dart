import 'dart:convert';

ListClassRoom listClassRoomFromJson(String str) =>
    ListClassRoom.fromJson(json.decode(str));

String listClassRoomToJson(ListClassRoom data) => json.encode(data.toJson());

class ListClassRoom {
  ListClassRoom({
    this.status,
    this.message,
    this.content,
    this.meta,
  });

  int status;
  String message;
  List<ContentClass> content;
  MetaPage meta;

  factory ListClassRoom.fromJson(Map<String, dynamic> json) => ListClassRoom(
        status: json["status"],
        message: json["message"],
        content: List<ContentClass>.from(
          json["content"].map(
            (x) => ContentClass.fromJson(x),
          ),
        ),
        meta: MetaPage.fromJson(json["meta"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "content": List<dynamic>.from(content.map((x) => x.toJson())),
        "meta": meta.toJson(),
      };
}

class ContentClass {
  ContentClass({
    this.id,
    this.name,
    this.totalStudent,
  });

  int id;
  String name;
  int totalStudent;

  factory ContentClass.fromJson(Map<String, dynamic> json) => ContentClass(
        id: json["id"],
        name: json["name"],
        totalStudent: json["total_student"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "total_student": totalStudent,
      };
}

class MetaPage {
  MetaPage({
    this.total,
    this.currentPage,
    this.lastPage,
    this.perPage,
  });

  int total;
  int currentPage;
  int lastPage;
  int perPage;

  factory MetaPage.fromJson(Map<String, dynamic> json) => MetaPage(
        total: json["total"],
        currentPage: json["current_page"],
        lastPage: json["last_page"],
        perPage: json["per_page"],
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "current_page": currentPage,
        "last_page": lastPage,
        "per_page": perPage,
      };
}
