import 'dart:convert';

UpdateProfile updateProfileFromJson(String str) =>
    UpdateProfile.fromJson(json.decode(str));

String updateProfileToJson(UpdateProfile data) => json.encode(data.toJson());

class UpdateProfile {
  UpdateProfile({
    this.fname,
    this.lname,
    this.gender,
    this.dob,
    this.address,
    this.email,
    this.religion,
    this.phoneNumber,
  });

  String fname;
  String lname;
  String gender;
  String dob;
  String address;
  String username;
  String email;
  String religion;
  int phoneNumber;
  factory UpdateProfile.fromJson(Map<String, dynamic> json) => UpdateProfile(
        fname: json["fname"],
        lname: json["lname"],
        gender: json["gender"],
        dob: json["dob"],
        address: json["address"],
        email: json["email"],
        religion: json["religion"],
        phoneNumber: json["phone_number"],
      );

  Map<String, dynamic> toJson() => {
        "fname": fname,
        "lname": lname,
        "gender": gender,
        "dob": dob,
        "address": address,
        "email": email,
        "religion": religion,
        "phone_number": phoneNumber,
      };
}
