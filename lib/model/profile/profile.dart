// To parse this JSON data, do
//
//     final profile = profileFromJson(jsonString);

import 'dart:convert';

Profile profileFromJson(String str) => Profile.fromJson(json.decode(str));

String profileToJson(Profile data) => json.encode(data.toJson());

class Profile {
  Profile({
    this.status,
    this.message,
    this.content,
    this.meta,
  });

  int status;
  String message;
  Content content;
  Meta meta;

  factory Profile.fromJson(Map<String, dynamic> json) => Profile(
        status: json["status"],
        message: json["message"],
        content: Content.fromJson(json["content"]),
        meta: Meta.fromJson(json["meta"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "content": content.toJson(),
        "meta": meta.toJson(),
      };
}

class Content {
  Content({
    this.name,
    this.nip,
    this.dob,
    this.address,
    this.email,
    this.schoolId,
    this.schoolName,
    this.gender,
    this.phoneNumber,
  });

  String name;
  String nip;
  String dob;
  String address;
  String email;
  int schoolId;
  String schoolName;
  String gender;
  String phoneNumber;

  factory Content.fromJson(Map<String, dynamic> json) => Content(
        name: json["name"],
        nip: json["nip"],
        dob: json["dob"],
        address: json["address"],
        email: json["email"],
        schoolId: json["school_id"],
        schoolName: json["school_name"],
        gender: json["gender"],
        phoneNumber: json["phone_number"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "nip": nip,
        "dob": dob,
        "address": address,
        "email": email,
        "school_id": schoolId,
        "school_name": schoolName,
        "gender": gender,
        "phone_number": phoneNumber,
      };
}

class Meta {
  Meta({
    this.total,
    this.currentPage,
    this.lastPage,
    this.perPage,
  });

  int total;
  int currentPage;
  int lastPage;
  int perPage;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        total: json["total"],
        currentPage: json["current_page"],
        lastPage: json["last_page"],
        perPage: json["per_page"],
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "current_page": currentPage,
        "last_page": lastPage,
        "per_page": perPage,
      };
}
