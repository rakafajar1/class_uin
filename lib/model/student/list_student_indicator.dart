class StudentIndicatorList {
  int status;
  String message;
  Content content;
  Meta meta;

  StudentIndicatorList({this.status, this.message, this.content, this.meta});

  StudentIndicatorList.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    content =
        json['content'] != null ? new Content.fromJson(json['content']) : null;
    meta = json['meta'] != null ? new Meta.fromJson(json['meta']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.content != null) {
      data['content'] = this.content.toJson();
    }
    if (this.meta != null) {
      data['meta'] = this.meta.toJson();
    }
    return data;
  }
}

class Content {
  int id;
  int studentId;
  String studentName;
  List<IndicatorScoreIds> indicatorScoreIds;
  dynamic totalIndicatorScore;

  Content({
    this.id,
    this.studentId,
    this.studentName,
    this.indicatorScoreIds,
    this.totalIndicatorScore,
  });

  Content.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    studentId = json['student_id'];
    studentName = json['student_name'];
    if (json['indicator_score_ids'] != null) {
      indicatorScoreIds = new List<IndicatorScoreIds>();
      json['indicator_score_ids'].forEach((v) {
        indicatorScoreIds.add(new IndicatorScoreIds.fromJson(v));
      });
    }
    totalIndicatorScore = json['total_indicator_score'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['student_id'] = this.studentId;
    data['student_name'] = this.studentName;
    if (this.indicatorScoreIds != null) {
      data['indicator_score_ids'] =
          this.indicatorScoreIds.map((v) => v.toJson()).toList();
    }
    data['total_indicator_score'] = this.totalIndicatorScore;
    return data;
  }
}

class IndicatorScoreIds {
  int id;
  int indicatorId;
  String indicatorName;
  double score;
  String note;

  IndicatorScoreIds({
    this.id,
    this.indicatorId,
    this.indicatorName,
    this.score,
    this.note,
  });

  IndicatorScoreIds.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    indicatorId = json['indicator_id'];
    indicatorName = json['indicator_name'];
    score = json['score'];
    note = json['note'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['indicator_id'] = this.indicatorId;
    data['indicator_name'] = this.indicatorName;
    data['score'] = this.score;
    data['note'] = this.note;
    return data;
  }
}

class Meta {
  int total;
  int currentPage;
  int lastPage;
  int perPage;

  Meta({this.total, this.currentPage, this.lastPage, this.perPage});

  Meta.fromJson(Map<String, dynamic> json) {
    total = json['total'];
    currentPage = json['current_page'];
    lastPage = json['last_page'];
    perPage = json['per_page'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total'] = this.total;
    data['current_page'] = this.currentPage;
    data['last_page'] = this.lastPage;
    data['per_page'] = this.perPage;
    return data;
  }
}
