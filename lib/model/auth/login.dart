class UserLogin {
  String username;
  String password;

  UserLogin({
    this.username,
    this.password,
  });

  factory UserLogin.fromJson(Map<String, dynamic> jsonObject) {
    return UserLogin(
      username: jsonObject['username'],
      password: jsonObject['password'],
    );
  }
}
