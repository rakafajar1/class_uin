
class UpdatePassword {
  final String newPassword;
  final String currentPassword;

  UpdatePassword({
    this.newPassword,
    this.currentPassword,
  });

  factory UpdatePassword.fromJson(Map<String, dynamic> jsonObject) {
    return UpdatePassword(
      newPassword: jsonObject['password'],
      currentPassword: jsonObject['copassword'],
    );
  }

  Map<String, dynamic> toJson() => {
        "password": newPassword,
        "copassword": currentPassword,
      };
}
