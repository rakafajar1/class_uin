import 'package:flutter/material.dart';

class MenuProfile {
  final String name;
  final IconData iconData;

  MenuProfile(this.name, this.iconData);
}
