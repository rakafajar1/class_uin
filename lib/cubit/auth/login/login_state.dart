part of 'login_cubit.dart';

abstract class LoginState extends Equatable {
  const LoginState();
}

class LoginInitial extends LoginState {
  @override
  List<Object> get props => [];
}

class LoginLoading extends LoginState {
  @override
  List<Object> get props => [];
}

class LoginSuccesState extends LoginState {
  final UserLogin userLogin;

  LoginSuccesState({this.userLogin});
  @override
  List<Object> get props => [userLogin];
}

class LoginFailedState extends LoginState {
  final String errorMessage;

  LoginFailedState({
    this.errorMessage,
  });

  @override
  List<Object> get props => [errorMessage];
}
