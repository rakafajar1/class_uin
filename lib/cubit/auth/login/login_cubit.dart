import 'package:bloc/bloc.dart';
import 'package:class_uin/model/auth/login.dart';
import 'package:class_uin/service/api_service.dart';
import 'package:class_uin/service/repository.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  ApiRepository _repository = ApiService();

  LoginCubit() : super(LoginInitial());

  // fungsi login
  Future<void> signIn({
    @required UserLogin userLogin,
  }) async {
    try {
      UserLogin login;
      // kondisi loading
      emit(
        LoginLoading(),
      );
      await _repository.fetchLogin(
        userLogin: UserLogin(
          username: userLogin.username,
          password: userLogin.password,
        ),
      );
      emit(
        LoginSuccesState(userLogin: login),
      );
    } catch (e) {
      emit(
        LoginFailedState(
          errorMessage: e.toString(),
        ),
      );
    }
  }
}
