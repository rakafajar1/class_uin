import 'package:bloc/bloc.dart';
import 'package:class_uin/model/auth/change_password.dart';
import 'package:class_uin/service/api_service.dart';
import 'package:class_uin/service/repository.dart';
import 'package:equatable/equatable.dart';

part 'change_password_state.dart';

class ChangePasswordCubit extends Cubit<ChangePasswordState> {
  ApiRepository _repository = ApiService();

  ChangePasswordCubit() : super(ChangePasswordInitial());

  Future<void> changePassword(
    UpdatePassword updatePassword,
  ) async {
    try {
      print('masuk');
      emit(
        ChangePasswordLoading(),
      );
      String message = await _repository.changePassword(
        updatePassword: UpdatePassword(
          newPassword: updatePassword.newPassword,
          currentPassword: updatePassword.currentPassword,
        ),
      );

      emit(
        ChangePasswordSucces(
          message: message,
        ),
      );
    } catch (e) {
      print(e);
      emit(
        ChangePasswordFailed(
          errorMessage: e.toString(),
        ),
      );
    }
  }
}
