part of 'change_password_cubit.dart';

abstract class ChangePasswordState extends Equatable {
  const ChangePasswordState();
}

class ChangePasswordInitial extends ChangePasswordState {
  @override
  List<Object> get props => [];
}

class ChangePasswordLoading extends ChangePasswordState {
  @override
  List<Object> get props => [];
}

class ChangePasswordSucces extends ChangePasswordState {
  final String message;

  ChangePasswordSucces({
    this.message,
  });

  @override
  List<Object> get props => [
        message,
      ];
}

class ChangePasswordFailed extends ChangePasswordState {
  final String errorMessage;

  ChangePasswordFailed({this.errorMessage});

  @override
  List<Object> get props => [errorMessage];
}
