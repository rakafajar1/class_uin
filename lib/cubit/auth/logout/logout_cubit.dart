import 'package:bloc/bloc.dart';
import 'package:class_uin/service/api_service.dart';
import 'package:class_uin/service/repository.dart';
import 'package:equatable/equatable.dart';

part 'logout_state.dart';

class LogoutCubit extends Cubit<LogoutState> {
  ApiRepository _repository = ApiService();
  LogoutCubit() : super(LogoutInitial());

  Future<void> logoutUser() async {
    try {
      emit(
        LogoutLoading(),
      );

      await _repository.fetchLogout();

      emit(
        LogoutSuccesState(),
      );
    } catch (e) {
      emit(
        LogoutFaildedState(
          errorMessage: e.toString(),
        ),
      );
    }
  }
}
