part of 'logout_cubit.dart';

abstract class LogoutState extends Equatable {
  const LogoutState();
}

class LogoutInitial extends LogoutState {
  @override
  List<Object> get props => [];
}

class LogoutLoading extends LogoutState {
  @override
  List<Object> get props => [];
}

class LogoutSuccesState extends LogoutState {
  final String successMessage;

  LogoutSuccesState({this.successMessage});

  @override
  List<Object> get props => [successMessage];
}

class LogoutFaildedState extends LogoutState {
  final String errorMessage;

  LogoutFaildedState({this.errorMessage});

  @override
  List<Object> get props => [errorMessage];
}
