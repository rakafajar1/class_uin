import 'package:bloc/bloc.dart';
import 'package:class_uin/model/profile/profile.dart';
import 'package:class_uin/model/profile/update_profile.dart';
import 'package:class_uin/service/api_service.dart';
import 'package:class_uin/service/repository.dart';
import 'package:equatable/equatable.dart';

part 'profile_state.dart';

class ProfileCubit extends Cubit<ProfileCubitState> {
  ApiRepository _repository = ApiService();
  ProfileCubit() : super(ProfileInitial());

  // Function change profille
  Future<void> changeProfile(UpdateProfile updateProfile) async {
    try {
      emit(ProfileLoading());
      String message = await _repository.changeProfile(
        updateProfile: UpdateProfile(
          fname: updateProfile.fname,
          lname: updateProfile.lname,
          gender: updateProfile.gender,
          dob: updateProfile.dob,
          address: updateProfile.address,
          email: updateProfile.email,
          religion: updateProfile.religion,
          phoneNumber: updateProfile.phoneNumber,
        ),
      );

      emit(
        ChangeProfileLoadMessageSuccess(
          message: message,
        ),
      );
    } catch (e) {
      emit(
        ProfileError(
          errorMessage: e.toString(),
        ),
      );
    }
  }
}
