part of 'profile_cubit.dart';

abstract class ProfileCubitState extends Equatable {
  const ProfileCubitState();
}

class ProfileInitial extends ProfileCubitState {
  @override
  List<Object> get props => [];
}

class ProfileLoading extends ProfileCubitState {
  @override
  List<Object> get props => [];
}

class ProfileLoaded extends ProfileCubitState {
  final Profile profile;

  ProfileLoaded({this.profile});

  @override
  List<Object> get props => [profile];
}

class ChangeProfileLoaded extends ProfileCubitState {
  final UpdateProfile updateProfile;

  ChangeProfileLoaded({this.updateProfile});

  @override
  List<Object> get props => [];
}

class ChangeProfileLoadMessageSuccess extends ProfileCubitState {
  final String message;

  ChangeProfileLoadMessageSuccess({this.message});

  @override
  List<Object> get props => [message];
}

class ProfileError extends ProfileCubitState {
  final String errorMessage;

  ProfileError({this.errorMessage});

  @override
  List<Object> get props => [errorMessage];
}
