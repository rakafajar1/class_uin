import 'dart:convert';

import 'package:class_uin/model/student/list_student_indicator.dart';
import 'package:class_uin/service/repository.dart';
import 'package:class_uin/utils/logging_interceptor.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:class_uin/model/auth/change_password.dart';
import 'package:class_uin/model/auth/login.dart';
import 'package:class_uin/model/class_room/detail_class_room.dart';
import 'package:class_uin/model/class_room/list_class_room.dart';
import 'package:class_uin/model/profile/profile.dart';
import 'package:class_uin/model/profile/update_profile.dart';
import 'package:class_uin/utils/extension.dart';

class ApiService implements ApiRepository {
  // Var global dio
  Dio get dio => _dio();
  static Response response;

  // set default configs
  Dio _dio() {
    final options = BaseOptions(
      baseUrl: 'http://103.153.2.153:8000/api/v1/',
      connectTimeout: 5000,
      receiveTimeout: 3000,
      contentType: 'application/json',
    );

    var dio = Dio(options);

    dio.interceptors.add(LogginInterceptors());

    return dio;
  }

  // SharedPredference ini digunakan untuk menyimpan token yang ada di API
  Future<String> getTokenPreference() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getString("token");
  }

  String _showException(final error, final stacktrace) {
    print("Exception occurred: $error stackTrace: $stacktrace");
    return "Failed Get Data";
  }

  @override
  Future<void> fetchLogin({UserLogin userLogin}) async {
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();

      response = await dio.post(
        'login',
        data: {
          "username": userLogin.username,
          "password": userLogin.password,
        },
      );
      var jsonObject = response.data;

      String token = jsonObject["content"]["token"];
      preferences.setString("token", token);
      preferences.setString("username", userLogin.username);
      preferences.setString("password", userLogin.password);
    } on DioError catch (e) {
      throw e.response.data["content"];
    } catch (error, stacktrace) {
      throw _showException(error, stacktrace);
    }
  }

  @override
  Future<String> fetchLogout() async {
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      String token = preferences.getString('token');
      response = await dio.post(
        'logout',
        options: Options(
          headers: {
            'Authorization': 'Token ' + token,
          },
        ),
      );
      if (response.statusCode == 200) {
        String message = response.data['content'];
        preferences.remove('token');
        return message;
      }
      String message = response.data['content'];
      return message;
    } on DioError catch (e) {
      throw e.getErrorMessage();
    } catch (error, stacktrace) {
      throw _showException(error, stacktrace);
    }
  }

  @override
  Future<String> changePassword({UpdatePassword updatePassword}) async {
    String token = await getTokenPreference();

    try {
      response = await dio.post(
        'edit-password',
        options: Options(
          headers: {
            'Authorization': 'Token ' + token,
          },
        ),
        data: {
          'password': updatePassword.newPassword,
          'copassword': updatePassword.currentPassword,
        },
      );

      String message = response.data['content'];
      return message;
    } on DioError catch (e) {
      throw e.response.data["content"];
    } catch (error, stacktrace) {
      throw _showException(error, stacktrace);
    }
  }

  @override
  Future<String> changeProfile({UpdateProfile updateProfile}) async {
    String token = await getTokenPreference();

    try {
      response = await dio.post(
        'edit-profile',
        options: Options(
          headers: {
            'Authorization': 'Token ' + token,
          },
        ),
        data:
          updateProfile.toJson(),

      );

      String message = response.data['content'];
      return message;
    } on DioError catch (e) {
      throw e.response.data["content"];
    } catch (error, stacktrace) {
      throw _showException(error, stacktrace);
    }
  }

  @override
  Future<Profile> fetchProfile() async {
    String token = await getTokenPreference();
    try {
      response = await dio.get(
        'profile',
        options: Options(
          headers: {
            'Authorization': 'Token ' + token,
          },
        ),
      );

      return Profile.fromJson(response.data);
    } on DioError catch (e) {
      throw e.getErrorMessage();
    } catch (error, stacktrace) {
      throw _showException(error, stacktrace);
    }
  }

  @override
  Future<ListClassRoom> getListClassRoom(
      {String page, String nameClass}) async {
    String token = await getTokenPreference();

    try {
      response = await dio.get(
        'classroom?page=$page'
        '&name=${nameClass == null ? '' : nameClass}',
        options: Options(
          headers: {
            'Authorization': 'Token ' + token,
          },
        ),
      );

      return ListClassRoom.fromJson(response.data);
    } on DioError catch (e) {
      throw e.getErrorMessage();
    } catch (error, stacktrace) {
      throw _showException(error, stacktrace);
    }
  }

  @override
  Future<ClassroomDetail> getDetailClassRoom({
    String page,
    String classRoomId,
  }) async {
    String token = await getTokenPreference();

    try {
      response = await dio.get(
        'classroom-line?classroom_id=$classRoomId&page=$page&name=',
        options: Options(
          headers: {
            'Authorization': 'Token ' + token,
          },
        ),
      );

      return ClassroomDetail.fromJson(response.data);
    } on DioError catch (e) {
      throw e.getErrorMessage();
    } catch (error, stacktrace) {
      throw _showException(error, stacktrace);
    }
  }

  @override
  Future<StudentIndicatorList> getListStudentIndicator({
    String idLine,
    String idStudent,
    String page,
  }) async {
    String token = await getTokenPreference();
    try {
      response = await dio.get(
        'student-indicators?page=$page'
        '&line_id=${idLine == null ? '' : idLine}'
        '&student_id=${idStudent == null ? '' : idStudent}',
        options: Options(
          headers: {
            'Authorization': 'Token ' + token,
          },
        ),
      );

      return StudentIndicatorList.fromJson(response.data);
    } on DioError catch (e) {
      throw e.getErrorMessage();
    } catch (error, stacktrace) {
      throw _showException(error, stacktrace);
    }
  }

  @override
  Future<String> putIndicatorToStudent({
    String idLine,
    String idIndicator,
    String scoreIndicator,
    String noteToStudent,
  }) async {
    String token = await getTokenPreference();
    try {
      response = await dio.put(
        'add-indicator-score',
        options: Options(
          headers: {
            'Authorization': 'Token ' + token,
          },
        ),
        data: {
          "score_ids": [
            {
              'id': idLine,
              'indicator_id': idIndicator,
              'score': scoreIndicator,
              'note': noteToStudent,
            },
          ],
        },
      );
      String message = response.data['content'];
      return message;
    } on DioError catch (e) {
      throw e.response.data["content"];
    } catch (error, stacktrace) {
      throw _showException(error, stacktrace);
    }
  }
}
