import 'package:class_uin/model/auth/change_password.dart';
import 'package:class_uin/model/auth/login.dart';
import 'package:class_uin/model/class_room/detail_class_room.dart';
import 'package:class_uin/model/class_room/list_class_room.dart';
import 'package:class_uin/model/profile/profile.dart';
import 'package:class_uin/model/profile/update_profile.dart';
import 'package:class_uin/model/student/list_student_indicator.dart';

abstract class ApiRepository {
  Future<void> fetchLogin({UserLogin userLogin});

  Future<String> fetchLogout();

  Future<Profile> fetchProfile();

  Future<String> changePassword({UpdatePassword updatePassword});

  Future<String> changeProfile({UpdateProfile updateProfile});

  Future<ListClassRoom> getListClassRoom({
    String page,
    String nameClass,
  });

  Future<ClassroomDetail> getDetailClassRoom({
    String page,
    String classRoomId,
  });

  Future<StudentIndicatorList> getListStudentIndicator({
    String idLine,
    String idStudent,
    String page,
  });

  Future<String> putIndicatorToStudent({
    String idLine,
    String idIndicator,
    String scoreIndicator,
    String noteToStudent,
  });
}
