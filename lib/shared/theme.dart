import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

const double defaultMargin = 24;

Color mainColor = Color(0xFFF5F5F3);
Color blackColor = Color(0xFF020202);
Color greyColor = Color(0xFF8D92A3);
Color greyColor1 = Color(0xFFFAFAFC);
Color accentColor1 = Color(0xFF2C1F63);
Color accentColor2 = Color(0xFFFBD460);
Color accentColor3 = Color(0xFFADADAD);
Color blueColor1 = Color(0xFF5765A3);

TextStyle appBarTextFont = TextStyle(
  fontSize: 25,
  fontFamily: 'Rowdies',
  fontWeight: FontWeight.w200,
  color: Colors.black,
);

TextStyle textBlackFont = TextStyle(
  color: Colors.black,
  fontFamily: 'Rowdies',
  fontSize: 12,
);

TextStyle textFontWeightNormal = GoogleFonts.poppins(
  fontWeight: FontWeight.normal,
);

TextStyle textFontWeightBold = GoogleFonts.poppins(
  fontWeight: FontWeight.bold,
);

TextStyle textFontWeight100 = GoogleFonts.poppins(
  fontWeight: FontWeight.w100,
);

TextStyle textFontWeight200 = GoogleFonts.poppins(
  fontWeight: FontWeight.w200,
);

TextStyle textFontWeight300 = GoogleFonts.poppins(
  fontWeight: FontWeight.w300,
);

TextStyle textFontWeight400 = GoogleFonts.poppins(
  fontWeight: FontWeight.w400,
);

TextStyle textFontWeight500 = GoogleFonts.poppins(
  fontWeight: FontWeight.w500,
);

TextStyle textFontWeight600 = GoogleFonts.poppins(
  fontWeight: FontWeight.w600,
);

TextStyle textFontWeight700 = GoogleFonts.poppins(
  fontWeight: FontWeight.w700,
);

TextStyle textFontWeight800 = GoogleFonts.poppins(
  fontWeight: FontWeight.w800,
);
TextStyle textFontWeight900 = GoogleFonts.poppins(
  fontWeight: FontWeight.w900,
);
