import 'package:class_uin/shared/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomeTextFieldTitle extends StatelessWidget {
  final String title;
  final EdgeInsetsGeometry margin;

  const CustomeTextFieldTitle({
    Key key,
    this.title,
    this.margin,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: margin,
      child: Text(
        title,
        style: textFontWeight500.copyWith(
          color: Colors.black,
          fontSize: 16.sp,
        ),
      ),
    );
  }
}

class CustomeTextField extends StatelessWidget {
  final TextEditingController controller;
  final TextStyle hintFieldStyle;
  final String hintFieldText;
  final bool obscureText;
  final TextInputType keyboardType;
  final String errorText;
  final FormFieldValidator<String> validator;
  final ValueChanged<String> onChanged;
  final bool enable;

  const CustomeTextField({
    Key key,
    this.controller,
    this.hintFieldStyle,
    this.hintFieldText,
    this.obscureText = false,
    this.keyboardType,
    this.errorText,
    this.validator,
    this.onChanged,
    this.enable = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(
        horizontal: defaultMargin,
      ),
      padding: EdgeInsets.symmetric(
        horizontal: 10,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        border: Border.all(
          color: Colors.black,
        ),
      ),
      child: TextFormField(
        validator: validator,
        keyboardType: keyboardType,
        obscureText: obscureText,
        controller: controller,
        decoration: InputDecoration(
          border: InputBorder.none,
          hintStyle: hintFieldStyle,
          hintText: hintFieldText,
          errorText: errorText,
        ),
        enabled: enable,
      ),
    );
  }
}
