import 'package:class_uin/shared/theme.dart';
import 'package:flutter/material.dart';

class AppBarCustom extends StatelessWidget {
  final String titleAppbar;
  final Widget body;

  final List<Widget> actions;
  final Widget leading;
  final bool centerTile;

  final ThemeData themeData;
  final PreferredSize preferredSize;

  const AppBarCustom({
    Key key,
    this.titleAppbar,
    this.body,
    this.actions,
    this.leading,
    this.centerTile,
    this.themeData,
    this.preferredSize,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: mainColor,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: mainColor,
        elevation: 0.0,
        title: Text(titleAppbar),
        actions: actions,
        centerTitle: centerTile,
        bottom: preferredSize,
      ),
      body: body,
    );
  }
}
