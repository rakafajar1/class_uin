import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class LoadingBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SpinKitCircle(
      color: Colors.blue,
      size: 50.0.sp,
    );
  }
}
