import 'package:class_uin/shared/theme.dart';
import 'package:class_uin/ui/widgets/custom_button.dart';
import 'package:class_uin/ui/widgets/reusable_text_form_field.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  TextEditingController emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            color: Colors.white,
          ),
          SafeArea(
            child: ListView(
              children: [
                Column(
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: defaultMargin,
                      ),
                      width: double.infinity,
                      height: 100.h,
                      color: Colors.white,
                      child: Row(
                        children: [
                          GestureDetector(
                            onTap: () {
                              Get.back();
                            },
                            child: Container(
                              width: 24.w,
                              height: 24.h,
                              margin: EdgeInsets.only(
                                right: 12,
                              ),
                              child: Icon(
                                Icons.arrow_back_ios,
                              ),
                              alignment: Alignment.center,
                            ),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'Lupa Password',
                                style: GoogleFonts.lobster(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 24.sp,
                                  color: blackColor,
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                    Container(
                      height: 24.h,
                      width: double.infinity,
                      color: greyColor1,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        CustomeTextFieldTitle(
                          title: 'Akun',
                          margin: EdgeInsets.fromLTRB(
                            defaultMargin,
                            26,
                            defaultMargin,
                            6,
                          ),
                        ),
                        CustomeTextField(
                          controller: emailController,
                          hintFieldStyle: TextStyle(
                            color: greyColor,
                          ),
                          hintFieldText: 'Masukan Akun Anda',
                        ),
                        CustomeButton(
                          margin: EdgeInsets.only(
                            top: defaultMargin,
                          ),
                          child: CustomeRaisedButton(
                            title: 'Lupa Password',
                            colorsButton: greyColor,
                            colorsText: Colors.white,
                            onPressed: () {
                              Get.to(
                                ForgotPassword(),
                              );
                            },
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
