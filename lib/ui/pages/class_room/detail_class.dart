import 'dart:async';
import 'package:class_uin/bloc/class_room/detail_class_room/detail_class_room_bloc.dart';
import 'package:class_uin/colors/colors.dart';
import 'package:class_uin/shared/theme.dart';
import 'package:class_uin/ui/pages/login/login_pages.dart';
import 'package:class_uin/ui/pages/student/list_student_indicator.dart';
import 'package:class_uin/ui/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class InitialDetailClassPage extends StatelessWidget {
  final String idClassRoom;
  final String namaKelas;

  const InitialDetailClassPage({
    Key key,
    this.idClassRoom,
    this.namaKelas,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<DetailClassRoomBloc>(
      create: (context) => DetailClassRoomBloc()
        ..add(
          GetDetailClassRoomFromApi(
            classRoomId: idClassRoom,
          ),
        ),
      child: DetailClassPage(
        idClassRoom: idClassRoom,
        namaKelas: namaKelas,
      ),
    );
  }
}

class DetailClassPage extends StatefulWidget {
  final String idClassRoom;
  final String namaKelas;

  const DetailClassPage({
    Key key,
    @required this.namaKelas,
    @required this.idClassRoom,
  }) : super(key: key);
  @override
  _DetailClassPageState createState() => _DetailClassPageState();
}

class _DetailClassPageState extends State<DetailClassPage> {
  // // Bloc
  DetailClassRoomBloc _classRoomBloc;

  // Refresh
  Completer<void> _refreshCompleter;

  ScrollController _scrollController = ScrollController();

  void _onScroll() {
    double maxScroll = _scrollController.position.maxScrollExtent;
    double currentScroll = _scrollController.position.pixels;
    if (maxScroll == currentScroll) {
      _classRoomBloc.add(
        GetPaginationDetailClassFromApi(
          classRoomId: widget.idClassRoom,
        ),
      );
    }
  }

  @override
  void initState() {
    super.initState();
    _classRoomBloc = BlocProvider.of<DetailClassRoomBloc>(context);
    // Refresh
    _refreshCompleter = Completer<void>();
    _scrollController.addListener(_onScroll);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: defaultMargin,
              ),
              width: double.infinity,
              height: 100.h,
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      Get.back();
                    },
                    child: Container(
                      width: 24.w,
                      height: 24.h,
                      margin: EdgeInsets.only(
                        right: 12,
                      ),
                      child: Icon(
                        Icons.arrow_back_ios,
                      ),
                      alignment: Alignment.center,
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        RichText(
                          text: TextSpan(
                            text: 'Kelas',
                            style: GoogleFonts.lobster(
                              fontWeight: FontWeight.w400,
                              fontSize: 24.sp,
                              color: blueColor1,
                            ),
                            children: <TextSpan>[
                              TextSpan(
                                text: 'Mu',
                                style: GoogleFonts.lobster(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 24.sp,
                                  color: blackColor,
                                ),
                              )
                            ],
                          ),
                        ),
                        Text(
                          widget.namaKelas,
                          style: textFontWeight300.copyWith(
                            color: greyColor,
                            fontSize: 14.sp,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      width: 40.w,
                      height: 40.h,
                      child: SvgPicture.asset(
                        'images/home_page.svg',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            BlocListener<DetailClassRoomBloc, DetailClassRoomState>(
              listener: (context, state) {
                if (state is DetailClassRoomLoadInProgress) {
                  _refreshCompleter?.complete();

                  _refreshCompleter = Completer();
                }
              },
              child: BlocBuilder<DetailClassRoomBloc, DetailClassRoomState>(
                builder: (context, state) {
                  if (state is DetailClassRoomLoadInProgress) {
                    return Expanded(
                      child: Center(
                        child: SpinKitRing(
                          color: blueColor1,
                          size: 48.sp,
                        ),
                      ),
                    );
                  }
                  if (state is DetailClassRoomLoadSuccess) {
                    return Expanded(
                      child: RefreshIndicator(
                        onRefresh: () {
                          _classRoomBloc
                            ..add(
                              GetDetailClassRoomFromApi(
                                classRoomId: widget.idClassRoom,
                              ),
                            );
                          return _refreshCompleter.future;
                        },
                        child: ListView.builder(
                          shrinkWrap: true,
                          controller: _scrollController,
                          padding: EdgeInsets.only(
                            bottom: 8,
                          ),
                          itemCount: state.hasReachedMax
                              ? state.contentDetailClass.length
                              : state.contentDetailClass.length,
                          itemBuilder: (context, int index) {
                            return Column(
                              children: [
                                InkWell(
                                  onTap: () {
                                    Get.to(
                                      InitialListStudentIndicatorPage(
                                        idStudent: state
                                            .contentDetailClass[index].studentId
                                            .toString(),
                                        idLine: state
                                            .contentDetailClass[index].id
                                            .toString(),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: double.infinity,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(
                                        width: 0.5,
                                      ),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(
                                          8,
                                        ),
                                      ),
                                    ),
                                    margin: EdgeInsets.only(
                                      top: 16.0,
                                      left: 8.0,
                                      right: 8.0,
                                    ),
                                    padding: EdgeInsets.only(
                                      left: 24.0,
                                      right: 24.0,
                                    ),
                                    height: 85.h,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              state.contentDetailClass[index]
                                                  .studentName,
                                              style: textBlackFont.copyWith(
                                                fontSize: 16.sp,
                                                fontWeight: FontWeight.w400,
                                              ),
                                            ),
                                            Text(
                                              'Indicator : ${state.contentDetailClass[index].indicators.toString()}',
                                              style: textBlackFont.copyWith(
                                                color: Colors.black45,
                                                fontSize: 12.sp,
                                                fontWeight: FontWeight.w300,
                                              ),
                                            ),
                                            Text(
                                              'Total Score : ${state.contentDetailClass[index].totalIndicatorScore.toInt()}',
                                              style: textBlackFont.copyWith(
                                                color: Colors.black45,
                                                fontSize: 12.sp,
                                                fontWeight: FontWeight.w300,
                                              ),
                                            ),
                                          ],
                                        ),
                                        Container(
                                          decoration: BoxDecoration(
                                            color: Colors.transparent,
                                            border: Border.all(
                                              width: 0.1,
                                            ),
                                            borderRadius: BorderRadius.all(
                                              Radius.circular(
                                                20,
                                              ),
                                            ),
                                          ),
                                          child: Icon(
                                            Icons.play_arrow,
                                            color: ThemeApp.primaryColor,
                                            size: 30.sp,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            );
                          },
                        ),
                      ),
                    );
                  }
                  if (state is DetailClassRoomLoadEmpty) {
                    return Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Center(
                            child: Text(
                              'Siswa masih kosong',
                              style: textFontWeight400.copyWith(
                                fontSize: 24.sp,
                                color: blackColor,
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  }
                  if (state is DetailClassRoomLoadError) {
                    return Expanded(
                      child: Container(
                        alignment: Alignment.center,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Center(
                              child: Text(state.message),
                            ),
                            CustomeButton(
                              margin: EdgeInsets.only(
                                top: defaultMargin,
                              ),
                              child: CustomeRaisedButton(
                                title: state.message ==
                                        'Anda belum melakukan login'
                                    ? 'Silahkan Login Kembali'
                                    : 'Segarkan',
                                colorsButton: blueColor1,
                                colorsText: Colors.white,
                                onPressed: () {
                                  state.message == 'Anda belum melakukan login'
                                      ? Get.offAll(
                                          InitialLoginPage(),
                                        )
                                      : _classRoomBloc.add(
                                          GetDetailClassRoomFromApi(
                                            classRoomId: widget.idClassRoom,
                                          ),
                                        );
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  }
                  return Container();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
