import 'dart:async';
import 'package:class_uin/bloc/class_room/list_class_room/list_class_room_bloc.dart';
import 'package:class_uin/shared/theme.dart';
import 'package:class_uin/ui/pages/class_room/detail_class.dart';
import 'package:class_uin/ui/pages/login/login_pages.dart';
import 'package:class_uin/ui/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class ListClassRoom extends StatefulWidget {
  @override
  _ListClassRoomState createState() => _ListClassRoomState();
}

class _ListClassRoomState extends State<ListClassRoom> {
  ListClassRoomBloc _listClassRoomBloc;

  String query;

  // Refresh
  Completer<void> _refreshCompleter;

  ScrollController _scrollController = ScrollController();

  void _onScroll() {
    double maxScroll = _scrollController.position.maxScrollExtent;
    double currentScroll = _scrollController.position.pixels;
    if (maxScroll == currentScroll) {
      _listClassRoomBloc.add(
        GetPaginationListClassFromApi(
          nameClass: query,
        ),
      );
    }
  }

  @override
  void initState() {
    _listClassRoomBloc = BlocProvider.of<ListClassRoomBloc>(context);
    // Refresh
    _refreshCompleter = Completer<void>();
    _scrollController.addListener(_onScroll);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ListClassRoomBloc, ListClassRoomState>(
      listener: (context, state) {
        if (state is ListClassRoomLoadInProgress) {
          _refreshCompleter?.complete();
          _refreshCompleter = Completer();
        }
      },
      child: Column(
        children: [
          Container(
            height: 60.h,
            width: double.infinity,
            color: greyColor1,
            child: TextFormField(
              onChanged: (newVal) {
                query = newVal;

                _listClassRoomBloc
                  ..add(
                    SearchListClassFromApi(nameClass: query),
                  );
              },
              cursorColor: Colors.black38,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.only(
                  left: 24,
                ),
                hintText: 'Cari Kelas',
                filled: true,
                fillColor: greyColor1,
                suffixIcon: Icon(
                  Icons.search,
                  color: Colors.black38,
                  size: 20.0.sp,
                ),
                border: InputBorder.none,
                hintStyle: textFontWeight200.copyWith(
                  fontSize: 20.sp,
                  color: Colors.black38,
                ),
              ),
              style: textFontWeight200.copyWith(
                fontSize: 20.sp,
                color: Colors.black38,
              ),
              textAlignVertical: TextAlignVertical.center,
            ),
          ),
          BlocBuilder<ListClassRoomBloc, ListClassRoomState>(
            builder: (context, state) {
              if (state is ListClassRoomLoadInProgress) {
                return Expanded(
                  child: Center(
                    child: SpinKitRing(
                      color: blueColor1,
                      size: 48.sp,
                    ),
                  ),
                );
              }
              if (state is ListClassRoomLoadSuccess) {
                return Expanded(
                  child: RefreshIndicator(
                    onRefresh: () {
                      _listClassRoomBloc
                        ..add(
                          GetListClassRoomFromApi(),
                        );
                      return _refreshCompleter.future;
                    },
                    child: ListView(
                      controller: _scrollController,
                      shrinkWrap: true,
                      children: [
                        GridView.builder(
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            mainAxisSpacing: 4,
                            crossAxisSpacing: 4,
                          ),
                          physics: ScrollPhysics(),
                          shrinkWrap: true,
                          primary: false,
                          scrollDirection: Axis.vertical,
                          itemCount: state.hasReachedMax
                              ? state.contentClass.length
                              : state.contentClass.length,
                          itemBuilder: (context, int index) {
                            return InkWell(
                              onTap: () {
                                Get.to(
                                  InitialDetailClassPage(
                                    namaKelas: state.contentClass[index].name,
                                    idClassRoom:
                                        state.contentClass[index].id.toString(),
                                  ),
                                );
                              },
                              child: Card(
                                elevation: 3,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(
                                    8,
                                  ),
                                ),
                                color: Colors.white,
                                child: Center(
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Text(
                                        state.contentClass[index].name,
                                        maxLines: 3,
                                        style: textFontWeight400.copyWith(
                                          fontSize: 24.sp,
                                          color: blackColor,
                                        ),
                                        textAlign: TextAlign.center,
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                );
              }
              if (state is ListClassRoomLoadEmpty) {
                return Expanded(
                  child: Center(
                    child: Text(
                      'Data kelas kosong',
                      style: textFontWeight400.copyWith(
                        fontSize: 24.sp,
                        color: blackColor,
                      ),
                    ),
                  ),
                );
              }
              if (state is ListClassRoomLoadError) {
                return Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Center(
                        child: Text(state.message),
                      ),
                      CustomeButton(
                        margin: EdgeInsets.only(
                          top: defaultMargin,
                        ),
                        child: CustomeRaisedButton(
                          title: state.message == 'Anda belum melakukan login'
                              ? 'Silahkan Login Kembali'
                              : 'Segarkan',
                          colorsButton: blueColor1,
                          colorsText: Colors.white,
                          onPressed: () {
                            state.message == 'Anda belum melakukan login'
                                ? Get.offAll(
                                    InitialLoginPage(),
                                  )
                                : _listClassRoomBloc.add(
                                    GetListClassRoomFromApi(),
                                  );
                          },
                        ),
                      ),
                    ],
                  ),
                );
              }
              return Container();
            },
          ),
        ],
      ),
    );
  }
}
