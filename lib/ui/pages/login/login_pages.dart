import 'package:class_uin/cubit/auth/login/login_cubit.dart';
import 'package:class_uin/model/auth/login.dart';
import 'package:class_uin/shared/theme.dart';
import 'package:class_uin/ui/pages/change_password/change_password.dart';
import 'package:class_uin/ui/pages/home/initial_home.dart';
import 'package:class_uin/ui/widgets/custom_button.dart';
import 'package:class_uin/ui/widgets/reusable_text_form_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class InitialLoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<LoginCubit>(
      create: (context) => LoginCubit(),
      child: LoginPage(),
    );
  }
}

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController userNameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  bool isEmailValid = false;
  bool isPasswordValid = false;
  bool isSignIn = false;
  bool _isHidePassword = true;

  // Membuat fungsi untuk melihat password
  void _togglePasswordVisibilty() {
    setState(() {
      _isHidePassword = !_isHidePassword;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: BlocListener<LoginCubit, LoginState>(
        listener: (context, state) async {
          if (state is LoginSuccesState) {
            Get.snackbar(
              '',
              '',
              backgroundColor: blueColor1,
              icon: Icon(
                Icons.info_outline,
                color: Colors.white,
              ),
              titleText: Text(
                'Login Successed',
                style: textFontWeight600.copyWith(
                  color: Colors.white,
                ),
              ),
              messageText: Text(
                'Wait a minute',
                style: textFontWeightNormal.copyWith(
                  color: Colors.white,
                ),
              ),
            );
            Future.delayed(
              const Duration(milliseconds: 2000),
              () {
                Get.offAll(
                  InitialHome(),
                );
              },
            );
          }
          if (state is LoginFailedState) {
            Get.snackbar(
              '',
              '',
              backgroundColor: Colors.blue[300],
              icon: Icon(
                Icons.info_outline,
                color: Colors.white,
              ),
              titleText: Text(
                'Login Failed',
                style: textFontWeight600.copyWith(
                  color: Colors.white,
                ),
              ),
              messageText: Text(
                state.errorMessage,
                style: textFontWeightNormal.copyWith(
                  color: Colors.white,
                ),
              ),
            );
          }
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: double.infinity,
              child: Column(
                children: [
                  RichText(
                    text: TextSpan(
                      text: 'E',
                      style: GoogleFonts.lobster(
                        fontWeight: FontWeight.w400,
                        fontSize: 42.sp,
                        color: blueColor1,
                      ),
                      children: <TextSpan>[
                        TextSpan(
                          text: '-ASSESMENT',
                          style: GoogleFonts.lobster(
                            fontWeight: FontWeight.w400,
                            fontSize: 42.sp,
                            color: blackColor,
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            CustomeTextFieldTitle(
              title: 'Akun',
              margin: EdgeInsets.fromLTRB(
                defaultMargin,
                26,
                defaultMargin,
                6,
              ),
            ),
            CustomeTextField(
              controller: userNameController,
              hintFieldStyle: TextStyle(
                color: greyColor,
              ),
              hintFieldText: 'Masukan Akun Anda',
            ),
            CustomeTextFieldTitle(
              title: 'Password',
              margin: EdgeInsets.fromLTRB(
                defaultMargin,
                16,
                defaultMargin,
                6,
              ),
            ),
            // CustomeTextField(
            //   controller: passwordController,
            //   hintFieldStyle: TextStyle(
            //     color: greyColor,
            //   ),
            //   obscureText: true,
            //   hintFieldText: 'Masukan Password Anda',
            // ),
            CustomeTextFieldPassword(
              controller: passwordController,
              keyboardType: TextInputType.text,
              obscureText: _isHidePassword,
              hintFieldStyle: TextStyle(
                color: greyColor,
              ),
              hintFieldText: 'Masukan Password',
              icon: Icon(
                _isHidePassword ? Icons.visibility : Icons.visibility_off,
                color: greyColor,
              ),
              onTap: () => _togglePasswordVisibilty(),
            ),
            CustomeButton(
              margin: EdgeInsets.only(
                top: defaultMargin,
              ),
              child: BlocBuilder<LoginCubit, LoginState>(
                builder: (context, state) {
                  return state is LoginLoading
                      ? SpinKitRing(
                          color: blueColor1,
                          size: 48.sp,
                        )
                      : CustomeRaisedButton(
                          title: 'Masuk',
                          colorsButton: blueColor1,
                          colorsText: Colors.white,
                          onPressed: () {
                            context.bloc<LoginCubit>().signIn(
                                  userLogin: UserLogin(
                                    username: userNameController.text,
                                    password: passwordController.text,
                                  ),
                                );
                          },
                        );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
