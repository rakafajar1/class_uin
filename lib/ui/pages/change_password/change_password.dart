import 'package:class_uin/cubit/auth/change_password/change_password_cubit.dart';
import 'package:class_uin/model/auth/change_password.dart';
import 'package:class_uin/shared/theme.dart';
import 'package:class_uin/ui/widgets/custom_button.dart';
import 'package:class_uin/ui/widgets/reusable_text_form_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';

class InitialChangePassword extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<ChangePasswordCubit>(
      create: (context) => ChangePasswordCubit(),
      child: ChangePassword(),
    );
  }
}

class ChangePassword extends StatefulWidget {
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  TextEditingController newPasswordController = TextEditingController();
  TextEditingController reTypePasswordController = TextEditingController();
  bool _isHidePasswordNew = true;
  bool _isHidePasswordReType = true;
  bool isNewPasswordValid = false;
  bool isReTypePasswordValid = false;

  // Membuat fungsi untuk melihat password
  void _toggleNewPasswordVisibilty() {
    setState(() {
      _isHidePasswordNew = !_isHidePasswordNew;
    });
  }

  void _toggleReTypePasswordVisibilty() {
    setState(() {
      _isHidePasswordReType = !_isHidePasswordReType;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<ChangePasswordCubit, ChangePasswordState>(
        listener: (context, state) {
          if (state is ChangePasswordFailed) {
            Get.snackbar(
              '',
              '',
              backgroundColor: Colors.blue[300],
              icon: Icon(
                Icons.info_outline,
                color: Colors.white,
              ),
              titleText: Text(
                'Change Password Failed',
                style: textFontWeight600.copyWith(
                  color: Colors.white,
                ),
              ),
              messageText: Text(
                state.errorMessage,
                style: textFontWeightNormal.copyWith(
                  color: Colors.white,
                ),
              ),
            );
          }
          if (state is ChangePasswordSucces) {
            Get.back();
            Get.snackbar(
              '',
              '',
              backgroundColor: blueColor1,
              icon: Icon(
                Icons.info_outline,
                color: Colors.white,
              ),
              titleText: Text(
                'Change Password Successed',
                style: textFontWeight600.copyWith(
                  color: Colors.white,
                ),
              ),
              messageText: Text(
                state.message,
                style: textFontWeightNormal.copyWith(
                  color: Colors.white,
                ),
              ),
            );
          }
        },
        child: Stack(
          children: [
            Container(
              color: Colors.white,
            ),
            SafeArea(
              child: ListView(
                children: [
                  Column(
                    children: [
                      Container(
                        padding: EdgeInsets.symmetric(
                          horizontal: defaultMargin,
                        ),
                        width: double.infinity,
                        height: 100,
                        color: Colors.white,
                        child: Row(
                          children: [
                            GestureDetector(
                              onTap: () {
                                Get.back();
                              },
                              child: Container(
                                width: 24,
                                height: 24,
                                margin: EdgeInsets.only(
                                  right: 12,
                                ),
                                child: Icon(
                                  Icons.arrow_back_ios,
                                ),
                                alignment: Alignment.center,
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'Ubah Password',
                                  style: textFontWeight500.copyWith(
                                    color: blackColor,
                                    fontSize: 24,
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                      Container(
                        height: 24,
                        width: double.infinity,
                        color: greyColor1,
                      ),
                      CustomeTextFieldTitle(
                        title: 'Password Baru',
                        margin: EdgeInsets.fromLTRB(
                          defaultMargin,
                          16,
                          defaultMargin,
                          6,
                        ),
                      ),
                      CustomeTextFieldPassword(
                        controller: newPasswordController,
                        keyboardType: TextInputType.text,
                        obscureText: _isHidePasswordNew,
                        hintFieldStyle: TextStyle(
                          color: greyColor,
                        ),
                        hintFieldText: 'Masukan Password Baru Anda',
                        onChanged: (text) {
                          setState(() {
                            isNewPasswordValid = text.isNotEmpty;
                          });
                        },
                        icon: Icon(
                          _isHidePasswordNew
                              ? Icons.visibility
                              : Icons.visibility_off,
                          color: greyColor,
                        ),
                        onTap: () => _toggleNewPasswordVisibilty(),
                      ),
                      CustomeTextFieldTitle(
                        title: 'Ulangi Password',
                        margin: EdgeInsets.fromLTRB(
                          defaultMargin,
                          16,
                          defaultMargin,
                          6,
                        ),
                      ),
                      CustomeTextFieldPassword(
                        controller: reTypePasswordController,
                        keyboardType: TextInputType.text,
                        obscureText: true,
                        hintFieldStyle: TextStyle(
                          color: greyColor,
                        ),
                        hintFieldText: 'Ulangi Password Baru Anda',
                        onChanged: (text) {
                          setState(() {
                            isReTypePasswordValid = text.isNotEmpty;
                          });
                        },
                        icon: Icon(
                          _isHidePasswordReType
                              ? Icons.visibility
                              : Icons.visibility_off,
                          color: greyColor,
                        ),
                        onTap: () => _toggleReTypePasswordVisibilty(),
                      ),
                      CustomeButton(
                        margin: EdgeInsets.only(
                          top: defaultMargin,
                        ),
                        child: BlocBuilder<ChangePasswordCubit,
                            ChangePasswordState>(
                          builder: (context, state) {
                            return state is ChangePasswordLoading
                                ? SpinKitRing(
                                    color: blueColor1,
                                    size: 48,
                                  )
                                : CustomeRaisedButton(
                                    title: 'Simpan Password',
                                    colorsButton: blueColor1,
                                    colorsText: Colors.white,
                                    onPressed: () {
                                      context
                                          .bloc<ChangePasswordCubit>()
                                          .changePassword(
                                            UpdatePassword(
                                              newPassword:
                                                  newPasswordController.text,
                                              currentPassword:
                                                  reTypePasswordController.text,
                                            ),
                                          );
                                    });
                          },
                        ),
                      ),
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class CustomeTextFieldPassword extends StatelessWidget {
  final TextEditingController controller;
  final TextStyle hintFieldStyle;
  final String hintFieldText;
  final bool obscureText;
  final TextInputType keyboardType;
  final String errorText;
  final FormFieldValidator<String> validator;
  final ValueChanged<String> onChanged;
  final bool enable;
  final VoidCallback onTap;
  final Icon icon;

  const CustomeTextFieldPassword({
    Key key,
    this.controller,
    this.hintFieldStyle,
    this.hintFieldText,
    this.obscureText = false,
    this.keyboardType,
    this.errorText,
    this.validator,
    this.onChanged,
    this.enable = true,
    this.icon,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(
        horizontal: defaultMargin,
      ),
      padding: EdgeInsets.symmetric(
        horizontal: 10,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        border: Border.all(
          color: Colors.black,
        ),
      ),
      child: TextFormField(
        validator: validator,
        keyboardType: keyboardType,
        obscureText: obscureText,
        controller: controller,
        decoration: InputDecoration(
          suffixIcon: GestureDetector(
            onTap: onTap,
            child: icon,
          ),
          border: InputBorder.none,
          hintStyle: hintFieldStyle,
          hintText: hintFieldText,
          errorText: errorText,
        ),
        enabled: enable,
      ),
    );
  }
}
