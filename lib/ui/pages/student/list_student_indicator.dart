import 'package:cached_network_image/cached_network_image.dart';
import 'package:class_uin/bloc/student/add_student/add_student_indicator_bloc.dart';
import 'package:class_uin/bloc/student/list_student_indicator/list_student_indicator_bloc.dart';
import 'package:class_uin/model/student/list_student_indicator.dart';
import 'package:class_uin/shared/theme.dart';
import 'package:class_uin/ui/widgets/custom_button.dart';
import 'package:class_uin/ui/widgets/loading_bar.dart';
import 'package:class_uin/ui/widgets/reusable_text_form_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class InitialListStudentIndicatorPage extends StatelessWidget {
  final String idLine;
  final String idStudent;

  const InitialListStudentIndicatorPage({
    Key key,
    this.idLine,
    this.idStudent,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ListStudentIndicatorBloc>(
          create: (context) => ListStudentIndicatorBloc()
            ..add(
              GetListStudentIndicatorFromApi(
                idLine: idLine,
                idStudent: idStudent,
              ),
            ),
        ),
        BlocProvider<AddStudentIndicatorBloc>(
          create: (context) => AddStudentIndicatorBloc(),
        ),
      ],
      child: ListStudentIndicatorPage(
        idLine: idLine,
        idStudent: idStudent,
      ),
    );
  }
}

class ListStudentIndicatorPage extends StatefulWidget {
  final String idLine;
  final String idStudent;

  const ListStudentIndicatorPage({Key key, this.idLine, this.idStudent})
      : super(key: key);
  @override
  _ListStudentIndicatorPageState createState() =>
      _ListStudentIndicatorPageState();
}

class _ListStudentIndicatorPageState extends State<ListStudentIndicatorPage> {
  ListStudentIndicatorBloc _listStudentIndicatorBloc;
  AddStudentIndicatorBloc _addStudentIndicatorBloc;
  StudentIndicatorList _studentIndicatorList;

  @override
  void initState() {
    super.initState();
    _listStudentIndicatorBloc =
        BlocProvider.of<ListStudentIndicatorBloc>(context);
    _addStudentIndicatorBloc =
        BlocProvider.of<AddStudentIndicatorBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: BlocListener<AddStudentIndicatorBloc, AddStudentIndicatorState>(
        listener: (context, state) {
          if (state is AddIndicatorToStudentLoadError) {
            Get.snackbar(
              '',
              '',
              backgroundColor: Colors.blue[300],
              icon: Icon(
                Icons.info_outline,
                color: Colors.white,
              ),
              titleText: Text(
                'Add Score Failed',
                style: textFontWeight600.copyWith(
                  color: Colors.white,
                ),
              ),
              messageText: Text(
                state.message,
                style: textFontWeightNormal.copyWith(
                  color: Colors.white,
                ),
              ),
            );
          }
          if (state is AddIndicatorToStudentLoadSuccess) {
            Get.snackbar(
              '',
              '',
              backgroundColor: blueColor1,
              icon: Icon(
                Icons.info_outline,
                color: Colors.white,
              ),
              titleText: Text(
                'Add Score Successed',
                style: textFontWeight600.copyWith(
                  color: Colors.white,
                ),
              ),
              messageText: Text(
                state.message,
                style: textFontWeightNormal.copyWith(
                  color: Colors.white,
                ),
              ),
            );
          }
        },
        child: SafeArea(
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: defaultMargin,
                ),
                width: double.infinity,
                height: 100.h,
                color: Colors.white,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Get.back();
                      },
                      child: Container(
                        width: 24.w,
                        height: 24.h,
                        margin: EdgeInsets.only(
                          right: 12,
                        ),
                        child: Icon(
                          Icons.arrow_back_ios,
                        ),
                        alignment: Alignment.center,
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          RichText(
                            text: TextSpan(
                              text: 'Murid',
                              style: GoogleFonts.lobster(
                                fontWeight: FontWeight.w400,
                                fontSize: 24.sp,
                                color: blueColor1,
                              ),
                              children: <TextSpan>[
                                TextSpan(
                                  text: 'Mu',
                                  style: GoogleFonts.lobster(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 24.sp,
                                    color: blackColor,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(
                        width: 40.w,
                        height: 40.h,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(
                            90,
                          ),
                          child: CachedNetworkImage(
                            height: 90.h,
                            width: 90.w,
                            imageUrl: '',
                            fit: BoxFit.cover,
                            placeholder: (context, url) => Container(
                              child: CircularProgressIndicator(),
                            ),
                            errorWidget: (context, url, error) => Center(
                              child: Icon(
                                Icons.people,
                                size: 64.sp,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              BlocBuilder<ListStudentIndicatorBloc, ListStudentIndicatorState>(
                builder: (context, state) {
                  if (state is ListStudentIndicatorLoadInProgress) {
                    return Expanded(
                      child: Center(
                        child: SpinKitRing(
                          color: blueColor1,
                          size: 48.sp,
                        ),
                      ),
                    );
                  }
                  if (state is ListStudentIndicatorLoadSuccess) {
                    // This list of controllers can be used to set and get the text from/to the TextFields
                    Map<String, TextEditingController> textEditingControllers =
                        {};

                    var textFields = <Widget>[];
                    state.studentIndicatorList.content.indicatorScoreIds
                        .forEach(
                      (str) {
                        var textEditingController = new TextEditingController(
                          text: '${str.score.toInt()}',
                        );
                        textEditingControllers.putIfAbsent(
                          str.indicatorName,
                          () => textEditingController,
                        );
                        return textFields.add(
                          Padding(
                            padding: const EdgeInsets.only(
                              bottom: 16,
                            ),
                            child: Column(
                              children: [
                                CustomeTextFieldTitle(
                                  title: str.indicatorName.capitalizeFirst,
                                  margin: EdgeInsets.only(
                                    left: 24,
                                    top: 8,
                                    right: 24,
                                    bottom: 4,
                                  ),
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      flex: 4,
                                      child: CustomeTextField(
                                        controller: textEditingController,
                                        hintFieldStyle: TextStyle(
                                          color: greyColor,
                                        ),
                                        hintFieldText: '0',
                                      ),
                                    ),
                                    Expanded(
                                      flex: 3,
                                      child: BlocBuilder<
                                          AddStudentIndicatorBloc,
                                          AddStudentIndicatorState>(
                                        builder: (context, state) {
                                          return Container(
                                            width: 32.w,
                                            height: 45.h,
                                            padding: EdgeInsets.symmetric(
                                              horizontal: defaultMargin,
                                            ),
                                            child: state
                                                    is AddIndicatorToStudentLoadInProgress
                                                ? SpinKitThreeBounce(
                                                    color: blueColor1,
                                                    size: 24.sp,
                                                  )
                                                : RaisedButton(
                                                    elevation: 0.0,
                                                    color: blueColor1,
                                                    shape:
                                                        RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                        8,
                                                      ),
                                                    ),
                                                    child: Text(
                                                      'Beri Nilai',
                                                      style: textFontWeight500
                                                          .copyWith(
                                                        color: Colors.white,
                                                        fontSize: 12.sp,
                                                      ),
                                                    ),
                                                    onPressed: () {
                                                      _addStudentIndicatorBloc
                                                        ..add(
                                                          PutIndicatorToStudentFromApi(
                                                            idLine: str.id
                                                                .toString(),
                                                            idIndicator: str
                                                                .indicatorId
                                                                .toString(),
                                                            scoreIndicator:
                                                                textEditingController
                                                                    .text,
                                                            noteToStudent:
                                                                'Good Raka',
                                                          ),
                                                        );
                                                    },
                                                  ),
                                          );
                                        },
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    );

                    return Expanded(
                      child: ListView(
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: textFields,
                          ),
                        ],
                      ),
                    );
                  }
                  if (state is ListStudentIndicatorLoadEmpty) {
                    return Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Center(
                            child: Text(
                              'Indikator masih kosong',
                              style: textFontWeight400.copyWith(
                                fontSize: 24.sp,
                                color: blackColor,
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  }

                  if (state is ListStudentIndicatorLoadError) {
                    return Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Center(
                            child: Text(state.message),
                          ),
                          CustomeButton(
                            margin: EdgeInsets.only(
                              top: defaultMargin,
                            ),
                            child: CustomeRaisedButton(
                              title: 'Segarkan',
                              colorsButton: blueColor1,
                              colorsText: Colors.white,
                              onPressed: () {
                                _listStudentIndicatorBloc
                                  ..add(
                                    GetListStudentIndicatorFromApi(
                                      idLine: widget.idLine,
                                      idStudent: widget.idStudent,
                                    ),
                                  );
                              },
                            ),
                          ),
                        ],
                      ),
                    );
                  }
                  return Container();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}
