import 'dart:async';

import 'package:class_uin/shared/theme.dart';
import 'package:class_uin/ui/pages/home/initial_home.dart';
import 'package:class_uin/ui/pages/login/login_pages.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  // Timer untuk splashScreen
  startSplashScreen() async {
    String token = await getTokenPreference();
    var duration = const Duration(
      seconds: 2,
    );
    return Timer(
      duration,
      () {
        Navigator.of(context).pushReplacement(
          MaterialPageRoute(
            builder: (_) => token != null ? InitialHome() : InitialLoginPage(),
          ),
        );
      },
    );
  }

  Future<String> getTokenPreference() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getString("token");
  }

  @override
  void initState() {
    super.initState();
    startSplashScreen();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body: Center(
        child: RichText(
          text: TextSpan(
            text: 'E',
            style: GoogleFonts.lobster(
              fontWeight: FontWeight.w400,
              fontSize: 48,
              color: blueColor1,
            ),
            children: <TextSpan>[
              TextSpan(
                text: '-ASSESMENT',
                style: GoogleFonts.lobster(
                  fontWeight: FontWeight.w400,
                  fontSize: 48,
                  color: blackColor,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
