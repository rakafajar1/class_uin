import 'package:class_uin/bloc/auth/log_out/logout_bloc.dart';
import 'package:class_uin/bloc/class_room/list_class_room/list_class_room_bloc.dart';
import 'package:class_uin/bloc/profile/profile_bloc.dart';
import 'package:class_uin/ui/pages/home/home_pages.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class InitialHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ProfileBloc>(
          create: (BuildContext context) => ProfileBloc()
            ..add(
              GetProfileFromApi(),
            ),
        ),
        BlocProvider<LogoutBloc>(
          create: (BuildContext context) => LogoutBloc(),
        ),
        BlocProvider<ListClassRoomBloc>(
          create: (context) => ListClassRoomBloc()
            ..add(
              GetListClassRoomFromApi(),
            ),
        )
      ],
      child: HomePage(),
    );
  }
}
