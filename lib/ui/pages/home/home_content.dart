import 'package:class_uin/shared/theme.dart';
import 'package:class_uin/ui/pages/class_room/list_class_room.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HomeContentPage extends StatefulWidget {
  @override
  _HomeContentPageState createState() => _HomeContentPageState();
}

class _HomeContentPageState extends State<HomeContentPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(
          130.0.h,
        ),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: defaultMargin,
              ),
              width: double.infinity,
              height: 150.h,
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    flex: 3,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        RichText(
                          text: TextSpan(
                            text: 'E',
                            style: GoogleFonts.lobster(
                              fontWeight: FontWeight.w400,
                              fontSize: 24.sp,
                              color: blueColor1,
                            ),
                            children: <TextSpan>[
                              TextSpan(
                                text: '-ASSESMENT',
                                style: GoogleFonts.lobster(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 24.sp,
                                  color: blackColor,
                                ),
                              )
                            ],
                          ),
                        ),
                        Text(
                          'Aplikasi Penilaian Siswa',
                          style: textFontWeight300.copyWith(
                            color: greyColor,
                            fontSize: 14.sp,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Container(
                      width: 90.w,
                      height: 90.h,
                      child: SvgPicture.asset(
                        'images/home_page.svg',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      body: ListClassRoom(),
    );
  }
}
