import 'package:class_uin/shared/theme.dart';
import 'package:class_uin/ui/pages/profile/profile_page.dart';
import 'package:flutter/material.dart';
import 'home_content.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int selectBottomNavBar = 0;

  final _widgetOptions = [
    HomeContentPage(),
    ProfilePage(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home_filled,
            ),
            label: 'Beranda',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.person,
            ),
            label: 'Profile',
          )
        ],
        type: BottomNavigationBarType.fixed,
        currentIndex: selectBottomNavBar,
        onTap: _onItemTapped,
        elevation: 0.0,
        backgroundColor: Colors.white,
        selectedItemColor: blueColor1,
        unselectedItemColor: greyColor,
      ),
      body: _widgetOptions.elementAt(selectBottomNavBar),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      selectBottomNavBar = index;
    });
  }
}
