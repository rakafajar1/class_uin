import 'dart:io';
import 'package:class_uin/cubit/auth/profile/profile_cubit.dart';
import 'package:class_uin/model/profile/profile.dart';
import 'package:class_uin/model/profile/update_profile.dart';
import 'package:class_uin/shared/theme.dart';
import 'package:class_uin/ui/widgets/custom_button.dart';
import 'package:class_uin/ui/widgets/reusable_text_form_field.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:image_picker/image_picker.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class InitialEditProfile extends StatelessWidget {
  final Profile profile;

  const InitialEditProfile({Key key, this.profile}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BlocProvider<ProfileCubit>(
      create: (context) => ProfileCubit(),
      child: EditProfile(
        profile: profile,
      ),
    );
  }
}

class EditProfile extends StatefulWidget {
  final Profile profile;

  const EditProfile({Key key, this.profile}) : super(key: key);

  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  String _valGender;
  List<Gender> _listGender = [
    Gender(
      gender: 'Laki-laki',
      sortGender: 'm',
    ),
    Gender(
      gender: 'Perempuan  ',
      sortGender: 'f',
    ),
  ];

  final _formKey = GlobalKey<FormState>();

  TextEditingController firstNameController;
  TextEditingController lastNameController;
  TextEditingController tanggalLahirController;
  TextEditingController addressController;
  TextEditingController emailController;
  TextEditingController noPhoneController;

  bool isFirstNameValid = false;
  bool isLastNameValid = false;
  bool isNIPValid = false;
  bool isTanggalLahirValid = false;
  bool isAddressValid = false;
  bool isEmailValid = false;
  bool isPhoneValid = false;

  // Image
  File image;
  final picker = ImagePicker();

  // Kalender
  DateTime selectedDate = DateTime.now();
  var convertDate = DateFormat('yyyy-MM-dd');
  _selectDate(BuildContext context) async {
    await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(1945),
      lastDate: DateTime(2090),
    ).then((selectedDate) {
      if (selectedDate != null) {
        tanggalLahirController.text = convertDate.format(selectedDate);
      }
    });
  }

  @override
  void initState() {
    super.initState();
    // Convert Nama
    var index = widget.profile.content.name.indexOf(' ');
    var nameFirst = widget.profile.content.name.substring(0, index);
    var nameLast = widget.profile.content.name.substring(index);

    firstNameController = TextEditingController(
      text: nameFirst,
    );
    lastNameController = TextEditingController(
      text: nameLast.trimLeft(),
    );
    addressController = TextEditingController(
      text: widget.profile.content.address,
    );
    tanggalLahirController = TextEditingController(
      text: widget.profile.content.dob,
    );
    noPhoneController = TextEditingController(
      text: widget.profile.content.phoneNumber,
    );
    emailController = TextEditingController(
      text: widget.profile.content.email,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<ProfileCubit, ProfileCubitState>(
        listener: (context, state) {
          if (state is ChangeProfileLoadMessageSuccess) {
            Get.back(
              result: widget.profile,
            );
            Get.snackbar(
              '',
              '',
              backgroundColor: blueColor1,
              icon: Icon(
                Icons.info_outline,
                color: Colors.white,
              ),
              titleText: Text(
                'Change Profile Successed',
                style: textFontWeight600.copyWith(
                  color: Colors.white,
                ),
              ),
              messageText: Text(
                state.message,
                style: textFontWeightNormal.copyWith(
                  color: Colors.white,
                ),
              ),
            );
          }
          if (state is ProfileError) {
            Get.snackbar(
              '',
              '',
              backgroundColor: Colors.blue[300],
              icon: Icon(
                Icons.info_outline,
                color: Colors.white,
              ),
              titleText: Text(
                'Change Profile Failed',
                style: textFontWeight600.copyWith(
                  color: Colors.white,
                ),
              ),
              messageText: Text(
                state.errorMessage,
                style: textFontWeightNormal.copyWith(
                  color: Colors.white,
                ),
              ),
            );
          }
        },
        child: Stack(
          children: [
            Container(
              color: Colors.white,
            ),
            SafeArea(
              child: ListView(
                padding: EdgeInsets.only(
                  bottom: 16,
                ),
                children: [
                  Column(
                    children: [
                      Container(
                        padding: EdgeInsets.symmetric(
                          horizontal: defaultMargin,
                        ),
                        width: double.infinity,
                        height: 100.h,
                        color: Colors.white,
                        child: Row(
                          children: [
                            GestureDetector(
                              onTap: () {
                                Get.back();
                              },
                              child: Container(
                                width: 24.w,
                                height: 24.h,
                                margin: EdgeInsets.only(
                                  right: 12,
                                ),
                                child: Icon(
                                  Icons.arrow_back_ios,
                                ),
                                alignment: Alignment.center,
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'Profile',
                                  style: textFontWeight500.copyWith(
                                    color: blackColor,
                                    fontSize: 24.sp,
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                      Container(
                        height: 24.h,
                        width: double.infinity,
                        color: greyColor1,
                      ),
                      Form(
                        key: _formKey,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            CustomeTextFieldTitle(
                              title: 'Nama Depan',
                              margin: EdgeInsets.fromLTRB(
                                defaultMargin,
                                16,
                                defaultMargin,
                                6,
                              ),
                            ),
                            CustomeTextField(
                              controller: firstNameController,
                              keyboardType: TextInputType.text,
                              hintFieldStyle: TextStyle(
                                color: greyColor,
                              ),
                              hintFieldText: 'Nama Depan Anda',
                              validator: validateFirstName,
                              onChanged: (text) {
                                setState(() {
                                  isFirstNameValid = text.isNotEmpty;
                                });
                              },
                            ),
                            CustomeTextFieldTitle(
                              title: 'Nama Belakang',
                              margin: EdgeInsets.fromLTRB(
                                defaultMargin,
                                16,
                                defaultMargin,
                                6,
                              ),
                            ),
                            CustomeTextField(
                              controller: lastNameController,
                              keyboardType: TextInputType.text,
                              hintFieldStyle: TextStyle(
                                color: greyColor,
                              ),
                              hintFieldText: 'Nama Belakang Anda',
                              validator: validateLastName,
                              onChanged: (text) {
                                setState(() {
                                  isLastNameValid = text.isNotEmpty;
                                });
                              },
                            ),
                            CustomeTextFieldTitle(
                              title: 'Tanggal Lahir',
                              margin: EdgeInsets.fromLTRB(
                                defaultMargin,
                                16,
                                defaultMargin,
                                6,
                              ),
                            ),
                            InkWell(
                              onTap: () => _selectDate(context),
                              child: CustomeTextField(
                                controller: tanggalLahirController,
                                keyboardType: TextInputType.datetime,
                                hintFieldStyle: TextStyle(
                                  color: greyColor,
                                ),
                                hintFieldText: 'Tanggal Lahir Anda',
                                enable: false,
                              ),
                            ),
                            CustomeTextFieldTitle(
                              title: 'Jenis Kelamin',
                              margin: EdgeInsets.fromLTRB(
                                defaultMargin,
                                16,
                                defaultMargin,
                                6,
                              ),
                            ),
                            Container(
                              width: double.infinity,
                              margin: EdgeInsets.symmetric(
                                horizontal: defaultMargin,
                              ),
                              padding: EdgeInsets.symmetric(
                                horizontal: 10,
                              ),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                border: Border.all(
                                  color: Colors.black,
                                ),
                              ),
                              child: DropdownButton(
                                hint: Text(
                                  widget.profile.content.gender,
                                ),
                                onChanged: (value) {
                                  setState(() {
                                    _valGender = value;
                                    print(_valGender);
                                  });
                                },
                                isExpanded: true,
                                value: _valGender,
                                underline: SizedBox(),
                                items: _listGender.map((value) {
                                  return DropdownMenuItem(
                                    child: Text(
                                      value.gender,
                                      style: textFontWeight500.copyWith(
                                        color: Colors.black,
                                      ),
                                    ),
                                    value: value.sortGender,
                                  );
                                }).toList(),
                              ),
                            ),
                            CustomeTextFieldTitle(
                              title: 'Email',
                              margin: EdgeInsets.fromLTRB(
                                defaultMargin,
                                16,
                                defaultMargin,
                                6,
                              ),
                            ),
                            CustomeTextField(
                              controller: emailController,
                              keyboardType: TextInputType.text,
                              hintFieldStyle: TextStyle(
                                color: greyColor,
                              ),
                              hintFieldText: 'Email Anda',
                              validator: (email) =>
                                  EmailValidator.validate(email)
                                      ? null
                                      : email.isEmpty == true
                                          ? 'Email Tidak Boleh Kosong'
                                          : 'Penulisan Email Salah',
                              onChanged: (text) {
                                setState(() {
                                  isEmailValid = text.isNotEmpty;
                                });
                              },
                            ),
                            CustomeTextFieldTitle(
                              title: 'No. Handphone',
                              margin: EdgeInsets.fromLTRB(
                                defaultMargin,
                                16,
                                defaultMargin,
                                6,
                              ),
                            ),
                            CustomeTextField(
                              controller: noPhoneController,
                              keyboardType: TextInputType.number,
                              hintFieldStyle: TextStyle(
                                color: greyColor,
                              ),
                              hintFieldText: 'No. Handphone Anda',
                              validator: validatePhone,
                              onChanged: (text) {
                                setState(() {
                                  isPhoneValid = text.isNotEmpty;
                                });
                              },
                            ),
                            CustomeTextFieldTitle(
                              title: 'Alamat',
                              margin: EdgeInsets.fromLTRB(
                                defaultMargin,
                                16,
                                defaultMargin,
                                6,
                              ),
                            ),
                            CustomeTextField(
                              controller: addressController,
                              keyboardType: TextInputType.text,
                              hintFieldStyle: TextStyle(
                                color: greyColor,
                              ),
                              hintFieldText: 'Alamat Anda',
                              validator: validateAddress,
                              onChanged: (text) {
                                setState(() {
                                  isAddressValid = text.isNotEmpty;
                                });
                              },
                            ),
                            CustomeButton(
                              margin: EdgeInsets.only(
                                top: defaultMargin,
                              ),
                              child:
                                  BlocBuilder<ProfileCubit, ProfileCubitState>(
                                builder: (context, state) {
                                  return state is ProfileLoading
                                      ? SpinKitRing(
                                          color: blueColor1,
                                          size: 48.sp,
                                        )
                                      : CustomeRaisedButton(
                                          title: 'Simpan Profile',
                                          colorsButton: blueColor1,
                                          colorsText: Colors.white,
                                          onPressed: () {
                                            context
                                                .bloc<ProfileCubit>()
                                                .changeProfile(
                                                  UpdateProfile(
                                                    fname: firstNameController
                                                        .text,
                                                    lname:
                                                        lastNameController.text,
                                                    dob: tanggalLahirController
                                                        .text,
                                                    address:
                                                        addressController.text,
                                                    email: emailController.text,
                                                    gender: _valGender != null
                                                        ? _valGender
                                                        : widget.profile.content
                                                                    .gender ==
                                                                'Laki-laki'
                                                            ? 'm'
                                                            : 'f',
                                                    religion: 'Islam',
                                                    phoneNumber: int.parse(
                                                      noPhoneController.text,
                                                    ),
                                                  ),
                                                );

                                            print(
                                                widget.profile.content.gender);
                                          },
                                        );
                                },
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  String validateFirstName(String value) {
    if (value.isEmpty) {
      return 'Nama Depan Tidak Boleh Kosong';
    } else {
      return null;
    }
  }

  String validateLastName(String value) {
    if (value.isEmpty) {
      return 'Nama Belakang Tidak Boleh Kosong';
    } else {
      return null;
    }
  }

  String validatePhone(String value) {
    if (value.isEmpty) {
      return 'No Handphone Tidak Boleh Kosong';
    } else {
      return null;
    }
  }

  String validateAddress(String value) {
    if (value.isEmpty) {
      return 'Alamat Tidak Boleh Kosong';
    } else {
      return null;
    }
  }
}

class Gender {
  final String gender;
  final String sortGender;

  Gender({this.gender, this.sortGender});
}
