import 'package:cached_network_image/cached_network_image.dart';
import 'package:class_uin/bloc/auth/log_out/logout_bloc.dart';
import 'package:class_uin/bloc/profile/profile_bloc.dart';
import 'package:class_uin/shared/theme.dart';
import 'package:class_uin/ui/pages/change_password/change_password.dart';
import 'package:class_uin/ui/pages/login/login_pages.dart';
import 'package:class_uin/ui/pages/profile/edit_profile.dart';
import 'package:class_uin/ui/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  ProfileBloc _profileBloc;
  LogoutBloc _logoutBloc;

  @override
  void initState() {
    super.initState();
    _profileBloc = BlocProvider.of<ProfileBloc>(context);
    _logoutBloc = BlocProvider.of<LogoutBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: BlocListener<LogoutBloc, LogoutState>(
        listener: (context, state) {
          if (state is LogOutLoadError) {
            Get.snackbar(
              '',
              '',
              backgroundColor: Colors.blue[300],
              icon: Icon(
                Icons.info_outline,
                color: Colors.white,
              ),
              titleText: Text(
                'Logout Failed',
                style: textFontWeight600.copyWith(
                  color: Colors.white,
                ),
              ),
              messageText: Text(
                state.message,
                style: textFontWeightNormal.copyWith(
                  color: Colors.white,
                ),
              ),
            );
          }
          if (state is LogOutLoadSuccess) {
            Get.snackbar(
              '',
              '',
              backgroundColor: blueColor1,
              icon: Icon(
                Icons.info_outline,
                color: Colors.white,
              ),
              titleText: Text(
                state.message,
                style: textFontWeight600.copyWith(
                  color: Colors.white,
                ),
              ),
              messageText: Text(
                'Wait a minute',
                style: textFontWeightNormal.copyWith(
                  color: Colors.white,
                ),
              ),
            );
            Future.delayed(
              const Duration(milliseconds: 1000),
              () {
                Get.offAll(
                  InitialLoginPage(),
                );
              },
            );
          }
        },
        child: BlocBuilder<ProfileBloc, ProfileState>(
          builder: (context, state) {
            if (state is ProfileLoadInProgress) {
              return Center(
                child: SpinKitRing(
                  color: blueColor1,
                  size: 48.sp,
                ),
              );
            }
            if (state is ProfileLoadSuccess) {
              return ListView(
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                    width: double.infinity,
                    height: 220.h,
                    color: Colors.white,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          alignment: Alignment.center,
                          height: 110.h,
                          width: 110.w,
                          margin: EdgeInsets.only(
                            bottom: 16,
                          ),
                          padding: EdgeInsets.all(16),
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage(
                                'images/photo_border.png',
                              ),
                            ),
                          ),
                          child: Container(
                            alignment: Alignment.center,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(
                                90,
                              ),
                              child: CachedNetworkImage(
                                height: 90.h,
                                width: 90.w,
                                imageUrl: '',
                                fit: BoxFit.cover,
                                placeholder: (context, url) => Container(
                                  child: CircularProgressIndicator(),
                                ),
                                errorWidget: (context, url, error) => Center(
                                  child: Icon(
                                    Icons.person_outline_sharp,
                                    size: 64,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Text(
                          state.profile.content.name,
                          style: textFontWeight500.copyWith(
                            fontSize: 18.sp,
                            color: blueColor1,
                          ),
                        ),
                        Text(
                          state.profile.content.nip,
                          style: textFontWeight300.copyWith(
                            fontSize: 14.sp,
                            color: greyColor,
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    height: 24.h,
                    width: double.infinity,
                    color: greyColor1,
                  ),
                  //// Body
                  Container(
                    width: double.infinity,
                    color: Colors.white,
                    child: Column(
                      children: [
                        InkWell(
                          onTap: () {
                            Get.to(
                              InitialEditProfile(
                                profile: state.profile,
                              ),
                            ).then((value) {
                              if (value != null) {
                                _profileBloc.add(
                                  GetProfileFromApi(),
                                );
                              }
                            });
                          },
                          child: Padding(
                            padding: EdgeInsets.only(
                              top: defaultMargin,
                              bottom: 16,
                              left: defaultMargin,
                              right: defaultMargin,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Ubah Profile',
                                  style: textFontWeight400.copyWith(
                                    fontSize: 14.sp,
                                    color: blackColor,
                                  ),
                                ),
                                SizedBox(
                                  height: 24.h,
                                  width: 24.w,
                                  child: Icon(
                                    Icons.arrow_forward_ios,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            Get.to(
                              InitialChangePassword(),
                            );
                          },
                          child: Padding(
                            padding: EdgeInsets.only(
                              bottom: 16,
                              left: defaultMargin,
                              right: defaultMargin,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Ubah Password',
                                  style: textFontWeight400.copyWith(
                                    fontSize: 14.sp,
                                    color: blackColor,
                                  ),
                                ),
                                SizedBox(
                                  height: 24.h,
                                  width: 24.w,
                                  child: Icon(
                                    Icons.arrow_forward_ios,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                            bottom: 16,
                            left: defaultMargin,
                            right: defaultMargin,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              BlocBuilder<LogoutBloc, LogoutState>(
                                builder: (context, state) {
                                  return state is LogOutLoadInProgress
                                      ? SpinKitThreeBounce(
                                          color: blueColor1,
                                          size: 12.sp,
                                        )
                                      : InkWell(
                                          onTap: () {
                                            _logoutBloc
                                              ..add(
                                                PostLogOutUser(),
                                              );
                                          },
                                          child: Text(
                                            'Keluar',
                                            style: textFontWeight400.copyWith(
                                              fontSize: 14.sp,
                                              color: Colors.red,
                                            ),
                                          ),
                                        );
                                },
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              );
            }
            if (state is ProfileLoadError) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Center(
                    child: Text(
                      state.message,
                    ),
                  ),
                  CustomeButton(
                    margin: EdgeInsets.only(
                      top: defaultMargin,
                    ),
                    child: CustomeRaisedButton(
                      title: state.message == 'Anda belum melakukan login'
                          ? 'Silahkan Login Kembali'
                          : 'Segarkan',
                      colorsButton: blueColor1,
                      colorsText: Colors.white,
                      onPressed: () {
                        state.message == 'Anda belum melakukan login'
                            ? Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                  builder: (BuildContext context) {
                                    return InitialLoginPage();
                                  },
                                ),
                                (r) {
                                  return false;
                                },
                              )
                            : _profileBloc.add(
                                GetProfileFromApi(),
                              );
                      },
                    ),
                  ),
                ],
              );
            }
            return Container();
          },
        ),
      ),
    );
  }
}
