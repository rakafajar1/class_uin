part of 'logout_bloc.dart';

abstract class LogoutState extends Equatable {
  const LogoutState();
}

class LogoutInitial extends LogoutState {
  @override
  List<Object> get props => [];
}

class LogOutLoadInProgress extends LogoutState {
  @override
  List<Object> get props => [];
}

class LogOutLoadSuccess extends LogoutState {
  final String message;

  LogOutLoadSuccess({this.message});
  @override
  List<Object> get props => [];
}

class LogOutLoadError extends LogoutState {
  final String message;

  LogOutLoadError({this.message});
  @override
  List<Object> get props => [];
}
