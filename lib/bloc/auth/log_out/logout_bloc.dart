import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:class_uin/service/api_service.dart';
import 'package:class_uin/service/repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'logout_event.dart';
part 'logout_state.dart';

class LogoutBloc extends Bloc<LogoutEvent, LogoutState> {
  LogoutBloc() : super(LogoutInitial());
  ApiRepository _repository = ApiService();

  @override
  Stream<LogoutState> mapEventToState(
    LogoutEvent event,
  ) async* {
    if (event is PostLogOutUser) {
      yield LogOutLoadInProgress();
      try {
        String message = await _repository.fetchLogout();
        yield LogOutLoadSuccess(
          message: message,
        );
      } catch (e) {
        yield LogOutLoadError(
          message: "$e",
        );
      }
    }
  }
}
