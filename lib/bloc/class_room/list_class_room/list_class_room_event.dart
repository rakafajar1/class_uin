part of 'list_class_room_bloc.dart';

abstract class ListClassRoomEvent extends Equatable {
  const ListClassRoomEvent();
}

class GetListClassRoomFromApi extends ListClassRoomEvent {
  final String nameClass;

  GetListClassRoomFromApi({this.nameClass});

  @override
  List<Object> get props => [];
}

class GetPaginationListClassFromApi extends ListClassRoomEvent {
  final String nameClass;

  GetPaginationListClassFromApi({this.nameClass});

  @override
  List<Object> get props => [];
}

class SearchListClassFromApi extends ListClassRoomEvent {
  final String nameClass;

  SearchListClassFromApi({this.nameClass});
  @override
  List<Object> get props => throw UnimplementedError();
}
