import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:class_uin/model/class_room/list_class_room.dart';
import 'package:class_uin/service/api_service.dart';
import 'package:class_uin/service/repository.dart';
import 'package:dio/dio.dart';

import 'package:equatable/equatable.dart';
import 'package:rxdart/rxdart.dart';
import 'package:class_uin/utils/extension.dart';
part 'list_class_room_event.dart';
part 'list_class_room_state.dart';

class ListClassRoomBloc extends Bloc<ListClassRoomEvent, ListClassRoomState> {
  ApiRepository _repository = ApiService();
  ListClassRoom listClassRoom;

  ListClassRoomBloc() : super(ListClassRoomInitial());

  @override
  Stream<Transition<ListClassRoomEvent, ListClassRoomState>> transformEvents(
    Stream<ListClassRoomEvent> events,
    TransitionFunction<ListClassRoomEvent, ListClassRoomState> transitionFn,
  ) {
    return super.transformEvents(
      events.debounceTime(const Duration(milliseconds: 500)),
      transitionFn,
    );
  }

  @override
  Stream<ListClassRoomState> mapEventToState(
    ListClassRoomEvent event,
  ) async* {
    ListClassRoomState currentState = state;
    if (event is GetListClassRoomFromApi) {
      yield ListClassRoomLoadInProgress();
      try {
        listClassRoom = await _repository.getListClassRoom(
          page: '1',
          nameClass: event.nameClass,
        );

        if (listClassRoom.content.length == 0) {
          yield ListClassRoomLoadEmpty();
        } else {
          yield ListClassRoomLoadSuccess(
            listClassRoom: listClassRoom,
            contentClass: listClassRoom.content,
            page: listClassRoom.meta.currentPage.toString(),
            hasReachedMax: listClassRoom.meta.total > 10 ? false : true,
          );
        }
      } catch (e) {
        yield ListClassRoomLoadError(
          message: "$e",
        );
      }
    } else if (event is SearchListClassFromApi) {
      listClassRoom = await _repository.getListClassRoom(
        page: "1",
        nameClass: event.nameClass,
      );

      if (listClassRoom.content.length == 0) {
        yield ListClassRoomLoadEmpty();
      } else {
        yield ListClassRoomLoadSuccess(
          listClassRoom: listClassRoom,
          contentClass: listClassRoom.content,
          page: listClassRoom.meta.currentPage.toString(),
          hasReachedMax: listClassRoom.meta.total > 10 ? false : true,
        );
      }
    } else if (event is GetPaginationListClassFromApi) {
      if (currentState is ListClassRoomLoadSuccess &&
          !currentState.hasReachedMax) {
        try {
          int currentPage = int.parse(currentState.page);
          listClassRoom = await _repository.getListClassRoom(
            page: "${currentPage + 1}",
            nameClass: event.nameClass,
          );
          print(currentPage);

          yield listClassRoom.content.isEmpty
              ? currentState.copyWith(
                  contentClass: currentState.contentClass,
                  listClassRoom: currentState.listClassRoom,
                  hasReachedMax: true,
                  page: listClassRoom.meta.currentPage.toString(),
                )
              : ListClassRoomLoadSuccess(
                  contentClass:
                      currentState.contentClass + listClassRoom.content,
                  listClassRoom: currentState.listClassRoom,
                  page: listClassRoom.meta.currentPage.toString(),
                  hasReachedMax: false,
                );
        } catch (e) {
          yield ListClassRoomLoadError(
            message: "$e",
          );
        }
      }
    }
  }
}
