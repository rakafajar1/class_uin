part of 'list_class_room_bloc.dart';

abstract class ListClassRoomState extends Equatable {
  const ListClassRoomState();
}

class ListClassRoomInitial extends ListClassRoomState {
  @override
  List<Object> get props => [];
}

class ListClassRoomLoadInProgress extends ListClassRoomState {
  @override
  List<Object> get props => [];
}

class ListClassRoomLoadSuccess extends ListClassRoomState {
  final ListClassRoom listClassRoom;
  final List<ContentClass> contentClass;
  final String page;
  final bool hasReachedMax;

  ListClassRoomLoadSuccess({
    this.listClassRoom,
    this.contentClass,
    this.page,
    this.hasReachedMax,
  });

  ListClassRoomLoadSuccess copyWith({
    final ListClassRoom listClassRoom,
    final List<ContentClass> contentClass,
    final String page,
    final bool hasReachedMax,
  }) {
    return ListClassRoomLoadSuccess(
      listClassRoom: listClassRoom,
      contentClass: contentClass,
      page: page,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
    );
  }

  @override
  List<Object> get props => [
        listClassRoom,
        contentClass,
        page,
        hasReachedMax,
      ];
}

class ListClassRoomLoadEmpty extends ListClassRoomState {
  @override
  List<Object> get props => [];
}

class ListClassRoomLoadError extends ListClassRoomState {
  final String message;

  ListClassRoomLoadError({this.message});

  @override
  List<Object> get props => [message];
}
