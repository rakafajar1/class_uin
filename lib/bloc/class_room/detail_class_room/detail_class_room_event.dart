part of 'detail_class_room_bloc.dart';

abstract class DetailClassRoomEvent extends Equatable {
  const DetailClassRoomEvent();
}

class GetDetailClassRoomFromApi extends DetailClassRoomEvent {
  final String classRoomId;

  GetDetailClassRoomFromApi({this.classRoomId});
  @override
  List<Object> get props => [classRoomId];
}

class GetPaginationDetailClassFromApi extends DetailClassRoomEvent {
  final String classRoomId;

  GetPaginationDetailClassFromApi({this.classRoomId});

  @override
  List<Object> get props => [];
}
