part of 'detail_class_room_bloc.dart';

abstract class DetailClassRoomState extends Equatable {
  const DetailClassRoomState();
}

class DetailClassRoomInitial extends DetailClassRoomState {
  @override
  List<Object> get props => [];
}

class DetailClassRoomLoadInProgress extends DetailClassRoomState {
  @override
  List<Object> get props => [];
}

class DetailClassRoomLoadSuccess extends DetailClassRoomState {
  final ClassroomDetail classRoomDetail;
  final List<ContentClassDetail> contentDetailClass;
  final String page;
  final bool hasReachedMax;

  DetailClassRoomLoadSuccess({
    this.classRoomDetail,
    this.contentDetailClass,
    this.page,
    this.hasReachedMax,
  });

  DetailClassRoomLoadSuccess copyWith({
    final ClassroomDetail classRoomDetail,
    final List<ContentClassDetail> contentDetailClass,
    final String page,
    final bool hasReachedMax,
  }) {
    return DetailClassRoomLoadSuccess(
      classRoomDetail: classRoomDetail,
      contentDetailClass: contentDetailClass,
      page: page,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
    );
  }

  @override
  List<Object> get props => [
        classRoomDetail,
        contentDetailClass,
        page,
        hasReachedMax,
      ];
}

class DetailClassRoomLoadEmpty extends DetailClassRoomState {
  @override
  List<Object> get props => [];
}

class DetailClassRoomLoadError extends DetailClassRoomState {
  final String message;

  DetailClassRoomLoadError({this.message});
  @override
  List<Object> get props => [
        message,
      ];
}
