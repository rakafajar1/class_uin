import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:class_uin/model/class_room/detail_class_room.dart';
import 'package:class_uin/service/api_service.dart';
import 'package:class_uin/service/repository.dart';

import 'package:equatable/equatable.dart';
import 'package:rxdart/rxdart.dart';

part 'detail_class_room_event.dart';
part 'detail_class_room_state.dart';

class DetailClassRoomBloc
    extends Bloc<DetailClassRoomEvent, DetailClassRoomState> {
  ClassroomDetail classRoomDetail;
  ApiRepository _repository = ApiService();

  DetailClassRoomBloc() : super(DetailClassRoomInitial());

  @override
  Stream<Transition<DetailClassRoomEvent, DetailClassRoomState>>
      transformEvents(
    Stream<DetailClassRoomEvent> events,
    TransitionFunction<DetailClassRoomEvent, DetailClassRoomState> transitionFn,
  ) {
    return super.transformEvents(
      events.debounceTime(const Duration(milliseconds: 500)),
      transitionFn,
    );
  }

  @override
  Stream<DetailClassRoomState> mapEventToState(
    DetailClassRoomEvent event,
  ) async* {
    DetailClassRoomState currentState = state;
    if (event is GetDetailClassRoomFromApi) {
      yield DetailClassRoomLoadInProgress();
      try {
        classRoomDetail = await _repository.getDetailClassRoom(
          page: '1',
          classRoomId: event.classRoomId,
        );

        if (classRoomDetail.content.length == 0) {
          yield DetailClassRoomLoadEmpty();
        } else {
          yield DetailClassRoomLoadSuccess(
            classRoomDetail: classRoomDetail,
            contentDetailClass: classRoomDetail.content,
            page: classRoomDetail.meta.currentPage.toString(),
            hasReachedMax: classRoomDetail.meta.total > 10 ? false : true,
          );
        }
      } catch (e) {
        yield DetailClassRoomLoadError(
          message: "$e",
        );
      }
    } else if (event is GetPaginationDetailClassFromApi) {
      if (currentState is DetailClassRoomLoadSuccess &&
          !currentState.hasReachedMax) {
        try {
          int currentPage = int.parse(currentState.page);
          classRoomDetail = await _repository.getDetailClassRoom(
            page: "${currentPage + 1}",
            classRoomId: event.classRoomId,
          );
          print(currentPage);

          yield classRoomDetail.content.isEmpty
              ? currentState.copyWith(
                  contentDetailClass: currentState.contentDetailClass,
                  classRoomDetail: currentState.classRoomDetail,
                  hasReachedMax: true,
                  page: classRoomDetail.meta.currentPage.toString(),
                )
              : DetailClassRoomLoadSuccess(
                  contentDetailClass:
                      currentState.contentDetailClass + classRoomDetail.content,
                  classRoomDetail: currentState.classRoomDetail,
                  page: classRoomDetail.meta.currentPage.toString(),
                  hasReachedMax: false,
                );
        } catch (e) {
          yield DetailClassRoomLoadError(
            message: "$e",
          );
        }
      }
    }
  }
}
