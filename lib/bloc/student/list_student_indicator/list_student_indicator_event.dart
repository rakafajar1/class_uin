part of 'list_student_indicator_bloc.dart';

abstract class ListStudentIndicatorEvent extends Equatable {
  const ListStudentIndicatorEvent();
}

class GetListStudentIndicatorFromApi extends ListStudentIndicatorEvent {
  final String idLine;
  final String idStudent;

  GetListStudentIndicatorFromApi({
    this.idLine,
    this.idStudent,
  });

  @override
  List<Object> get props => [];
}
