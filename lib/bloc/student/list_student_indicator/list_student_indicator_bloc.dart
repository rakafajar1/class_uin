import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:class_uin/model/student/list_student_indicator.dart';
import 'package:class_uin/service/api_service.dart';
import 'package:class_uin/service/repository.dart';
import 'package:equatable/equatable.dart';
part 'list_student_indicator_event.dart';
part 'list_student_indicator_state.dart';

class ListStudentIndicatorBloc
    extends Bloc<ListStudentIndicatorEvent, ListStudentIndicatorState> {
  ListStudentIndicatorBloc() : super(ListStudentIndicatorInitial());
  ApiRepository _repository = ApiService();
  StudentIndicatorList studentIndicatorList;

  @override
  Stream<ListStudentIndicatorState> mapEventToState(
    ListStudentIndicatorEvent event,
  ) async* {
    if (event is GetListStudentIndicatorFromApi) {
      yield ListStudentIndicatorLoadInProgress();
      try {
        studentIndicatorList = await _repository.getListStudentIndicator(
          idLine: event.idLine,
          idStudent: event.idStudent,
        );

        if (studentIndicatorList.content.indicatorScoreIds.length == 0) {
          yield ListStudentIndicatorLoadEmpty();
        } else {
          yield ListStudentIndicatorLoadSuccess(
            studentIndicatorList: studentIndicatorList,
          );
        }
      } catch (e) {
        yield ListStudentIndicatorLoadError(
          message: "$e",
        );
      }
    }
  }
}
