part of 'list_student_indicator_bloc.dart';

abstract class ListStudentIndicatorState extends Equatable {
  const ListStudentIndicatorState();
}

class ListStudentIndicatorInitial extends ListStudentIndicatorState {
  @override
  List<Object> get props => [];
}

class ListStudentIndicatorLoadInProgress extends ListStudentIndicatorState {
  @override
  List<Object> get props => [];
}

class ListStudentIndicatorLoadEmpty extends ListStudentIndicatorState {
  @override
  List<Object> get props => [];
}

class ListStudentIndicatorLoadSuccess extends ListStudentIndicatorState {
  final StudentIndicatorList studentIndicatorList;

  ListStudentIndicatorLoadSuccess({
    this.studentIndicatorList,
  });

  @override
  List<Object> get props => [
        studentIndicatorList,
      ];
}

class ListStudentIndicatorLoadError extends ListStudentIndicatorState {
  final String message;

  ListStudentIndicatorLoadError({this.message});
  @override
  List<Object> get props => [];
}
