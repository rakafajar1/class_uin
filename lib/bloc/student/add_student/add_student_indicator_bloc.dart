import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:class_uin/service/api_service.dart';
import 'package:class_uin/service/repository.dart';
part 'add_student_indicator_event.dart';
part 'add_student_indicator_state.dart';

class AddStudentIndicatorBloc
    extends Bloc<AddStudentIndicatorEvent, AddStudentIndicatorState> {
  AddStudentIndicatorBloc() : super(AddStudentIndicatorInitial());
  ApiRepository _repository = ApiService();

  @override
  Stream<AddStudentIndicatorState> mapEventToState(
    AddStudentIndicatorEvent event,
  ) async* {
    if (event is PutIndicatorToStudentFromApi) {
      yield AddIndicatorToStudentLoadInProgress();
      try {
        String message = await _repository.putIndicatorToStudent(
          idIndicator: event.idIndicator,
          idLine: event.idLine,
          scoreIndicator: event.scoreIndicator,
          noteToStudent: event.noteToStudent,
        );

        yield AddIndicatorToStudentLoadSuccess(
          message: message,
        );
      } catch (e) {
        yield AddIndicatorToStudentLoadError(
          message: "$e",
        );
      }
    }
  }
}
