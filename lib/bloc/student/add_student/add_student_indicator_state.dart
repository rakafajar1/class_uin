part of 'add_student_indicator_bloc.dart';

abstract class AddStudentIndicatorState extends Equatable {
  const AddStudentIndicatorState();
}

class AddStudentIndicatorInitial extends AddStudentIndicatorState {
  @override
  List<Object> get props => [];
}

class AddIndicatorToStudentLoadInProgress extends AddStudentIndicatorState {
  @override
  List<Object> get props => [];
}

class AddIndicatorToStudentLoadSuccess extends AddStudentIndicatorState {
  final String message;

  AddIndicatorToStudentLoadSuccess({this.message});

  @override
  List<Object> get props => [message];
}

class AddIndicatorToStudentLoadError extends AddStudentIndicatorState {
  final String message;

  AddIndicatorToStudentLoadError({this.message});
  @override
  List<Object> get props => [message];
}
