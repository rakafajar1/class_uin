part of 'add_student_indicator_bloc.dart';

abstract class AddStudentIndicatorEvent extends Equatable {
  const AddStudentIndicatorEvent();
}

class PutIndicatorToStudentFromApi extends AddStudentIndicatorEvent {
  final String idLine;
  final String idIndicator;
  final String scoreIndicator;
  final String noteToStudent;

  PutIndicatorToStudentFromApi({
    this.idLine,
    this.idIndicator,
    this.scoreIndicator,
    this.noteToStudent,
  });

  @override
  List<Object> get props => [
        idLine,
        idIndicator,
        scoreIndicator,
        noteToStudent,
      ];
}
