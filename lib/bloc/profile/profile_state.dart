part of 'profile_bloc.dart';

abstract class ProfileState extends Equatable {
  const ProfileState();
}

class ProfileInitial extends ProfileState {
  @override
  List<Object> get props => [];
}

class ProfileLoadInProgress extends ProfileState {
  @override
  List<Object> get props => [];
}

class ProfileLoadSuccess extends ProfileState {
  final Profile profile;

  ProfileLoadSuccess({this.profile});
  @override
  List<Object> get props => [profile];
}

class ProfileLoadError extends ProfileState {
  final String message;

  ProfileLoadError({this.message});
  @override
  List<Object> get props => [message];
}
