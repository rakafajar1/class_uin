import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:class_uin/model/profile/profile.dart';
import 'package:class_uin/service/api_service.dart';
import 'package:class_uin/service/repository.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';

part 'profile_event.dart';
part 'profile_state.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  ProfileBloc() : super(ProfileInitial());
  ApiRepository _repository = ApiService();
  Profile profile;

  @override
  Stream<ProfileState> mapEventToState(
    ProfileEvent event,
  ) async* {
    if (event is GetProfileFromApi) {
      yield ProfileLoadInProgress();
      try {
        profile = await _repository.fetchProfile();

        yield ProfileLoadSuccess(
          profile: profile,
        );
      } catch (e) {
        yield ProfileLoadError(
          message: "$e",
        );
      }
    }
  }
}
