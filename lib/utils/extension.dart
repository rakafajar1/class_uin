import 'package:dio/dio.dart';

extension DioErrorX on DioError {
  String getErrorMessage() {
    String _message = "Terjadi kesalahan pada server";

    if (this.type == DioErrorType.RESPONSE) {
      if (this.response.statusCode == 401) {
        _message = "Anda belum melakukan login";
      }
    } else if (this.type == DioErrorType.CONNECT_TIMEOUT)
      _message = "Connection timeout";
    else if (this.type == DioErrorType.DEFAULT)
      _message = "Koneksi tidak terhubung";

    return _message;
  }
}
