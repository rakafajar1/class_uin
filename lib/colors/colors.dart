import 'package:flutter/material.dart';

class ThemeApp {
  ThemeApp._();
  static const Color primaryColor = Color(0xFF2889cc);
  static const Color orange = Color(0xFFF4B932);
  static const Color secondaryColor = Color(0xFFa37b24);
  static const Color borderColor = Color(0xFFA8A8A8);
  static const Color successfullyColor = Color(0xFF024C1F);
  static const Color disableButton = Color(0xFFADADAD);
}
